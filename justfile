# just dump-db
dump-db:
    #!/bin/bash
    echo -e "Dumping datbase..."
    docker compose exec -T -u root database mariadb-dump --skip-lock-tables --no-tablespaces ${MARIADB_DATABASE:-kycnot} > dump.sql && \
    echo -e "Dump done!"

# Import a database dump: just import-db /path/to/dump.sql
import-db dump="":
    #!/bin/bash
    echo -e "Importing database..."
    docker compose exec -iT -u root database mariadb ${MARIADB_DATABASE:-kycnot} < {{dump}} && \
    echo -e "Done!"

# Install all dependencies
install:
    #!/bin/bash
    if [ {{ os() }} == "macos" ]; then
        # Install just
        brew install just

        # Install go packages
        cd {{justfile_directory()}}/src
        brew install go
        go mod tidy
        go install github.com/a-h/templ/cmd/templ@latest
        go install github.com/mitranim/gow@latest
        cd {{justfile_directory()}}

        # Install docker and database
        brew install docker
        cd {{justfile_directory()}}
        docker compose up database -d
        just import-db dump.sql
        docker compose down

        # Install frontend dependencies
        cd {{justfile_directory()}}/src/frontend
        curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh | bash
        source $HOME/.nvm/nvm.sh
        nvm install
        npm i
        cd {{justfile_directory()}}
    fi

# Start developing
develop:
    #!/bin/bash
    trap 'kill 0' SIGINT SIGTERM
    (cd {{justfile_directory()}}/src/frontend && npm i && npm run tailwind:dev) &
    (cd {{justfile_directory()}}/src/frontend/templates && templ generate -watch) &
    (cd {{justfile_directory()}}/src && gow run . serve -dev) &
    (cd {{justfile_directory()}} && just docker up database) &
    wait

dev +command:
    docker compose -f docker-compose.yml -f docker-compose.dev.yml {{command}}