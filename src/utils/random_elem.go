package utils

import (
	"time"

	"golang.org/x/exp/rand"
)

var r = rand.New(rand.NewSource(uint64(time.Now().Unix())))

func GetRandomString(slice []string) string {
	return slice[r.Intn(len(slice))]
}
