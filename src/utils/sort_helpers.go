package utils

import "kycnot.me/internal/ent/attribute"

func SortHelperAttributeRating(rating attribute.Rating) int {
	switch rating {
	case attribute.RatingGood:
		return 0
	case attribute.RatingInfo:
		return 1
	case attribute.RatingWarn:
		return 2
	case attribute.RatingBad:
		return 3
	default:
		return 4
	}
}
