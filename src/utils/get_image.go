package utils

import (
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/nfnt/resize"
	"github.com/phuslu/log"
	"golang.org/x/image/bmp"
	"golang.org/x/image/tiff"
	"kycnot.me/internal/cache"
)

// maxConcurrentDownloads limits the number of concurrent image downloads
const maxConcurrentDownloads = 10

var downloadSemaphore = make(chan struct{}, maxConcurrentDownloads)

func GetImage(url, slug string) {
	cacheKey := fmt.Sprintf("%s-imgc", slug)

	if _, found := cache.Cache.Get(cacheKey); found {
		return
	}

	outputDir := filepath.Join(Getenv("ROOT_PATH", "./"), "frontend/static/images")
	outputPath := filepath.Join(outputDir, slug+".jpg")

	if info, err := os.Stat(outputPath); err == nil && time.Since(info.ModTime()) < 30*24*time.Hour {
		log.Debug().Msg("Image is up to date, skipping download")
		cache.Cache.SetWithTTL(cacheKey, outputPath, 1, 24*30*time.Hour)
		return
	}

	log.Debug().Msgf("Getting image... %s", url)

	downloadSemaphore <- struct{}{}
	defer func() { <-downloadSemaphore }()

	resp, err := http.Get(url)
	if err != nil {
		log.Error().Err(err).Msg("Failed to download image")
		return
	}
	defer resp.Body.Close()

	img, format, err := image.Decode(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("Failed to decode image")
		return
	}

	log.Debug().Msgf("Image format: %s", format)

	resizedImg := resize.Resize(128, 128, img, resize.Lanczos3)

	if err := os.MkdirAll(outputDir, 0o755); err != nil {
		log.Error().Err(err).Msg("Failed to create output directory")
		return
	}

	tempFile := getTempFile(outputDir)
	if tempFile == nil {
		return
	}
	defer os.Remove(tempFile.Name())
	defer tempFile.Close()

	err = jpeg.Encode(tempFile, resizedImg, &jpeg.Options{Quality: 50})
	if err != nil {
		log.Error().Err(err).Msg("Failed to encode and save image as JPEG")
		return
	}

	if err := os.Rename(tempFile.Name(), outputPath); err != nil {
		log.Error().Err(err).Msg("Failed to rename temporary file")
		return
	}

	cache.Cache.SetWithTTL(cacheKey, outputPath, 1, 24*30*time.Hour)
	log.Info().Msg("Image processing completed successfully")
}

func getTempFile(dir string) *os.File {
	tempFile, err := os.CreateTemp(dir, "temp_*.jpg")
	if err != nil {
		log.Error().Err(err).Msg("Failed to create temporary file")
		return nil
	}

	return tempFile
}

func init() {
	image.RegisterFormat("jpeg", "jpeg", jpeg.Decode, jpeg.DecodeConfig)
	image.RegisterFormat("png", "png", png.Decode, png.DecodeConfig)
	image.RegisterFormat("bmp", "bmp", bmp.Decode, bmp.DecodeConfig)
	image.RegisterFormat("tiff", "tiff", tiff.Decode, tiff.DecodeConfig)
}
