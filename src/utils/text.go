package utils

import "strings"

// TODO; LIne Clamp
func ShortText(s string) string {
	if len(s) > 50 {
		return strings.TrimSpace(s[:50]) + "..."
	}
	return s
}
