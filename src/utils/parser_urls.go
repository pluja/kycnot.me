package utils

import "strings"

func UrlParser(url string) string {
	url = strings.ReplaceAll(url, " ", "")

	if !strings.HasPrefix(url, "http://") && !strings.HasPrefix(url, "https://") {
		url = "https://" + url
	}

	return url
}

func UrlListParser(urls string) []string {
	if urls == "" {
		return []string{}
	}

	url_list := strings.Split(strings.ReplaceAll(urls, " ", ""), ",")

	// Check all urls for http:// or https://, if not present, add it
	for i, url := range url_list {
		if !strings.HasPrefix(url, "http://") && !strings.HasPrefix(url, "https://") {
			url_list[i] = "https://" + url
		}
	}

	return url_list
}
