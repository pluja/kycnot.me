package utils

import (
	"fmt"
	"math"
	"strconv"
	"time"

	"kycnot.me/internal/cache"
	"kycnot.me/internal/ent"
)

const (
	baseScore       = 10.0
	maxScore        = 10.0
	minScore        = 0.0
	verifiedBonus   = 0.3
	p2pBonus        = 0.5
	openSourceBonus = 0.2
	torBonus        = 0.5
	cashBTCBonus    = 0.1
	xmrBonus        = 0.2
)

var kycLevelScores = map[int]float64{
	0: 8.0,
	1: 6.5,
	2: 6.0,
	3: 5.0,
}

var attributeScores = map[string]float64{
	"good": 0.1,
	"warn": -0.2,
	"bad":  -0.4,
}

type ServiceScore struct {
	Value   int
	Summary string
}

func ComputeScore(s *ent.Service) ServiceScore {
	cacheKey := fmt.Sprintf("%s-score", s.ID)
	cachedScore, found := cache.Cache.Get(cacheKey)
	if found {
		if cs, ok := cachedScore.(ServiceScore); ok {
			return cs
		}
	}

	summary := fmt.Sprintf("SCORE BREAKDOWN - %s:\n", s.Name)
	summary += "\n||-----------------------------------------------"

	grade := calculateBaseScore(s, &summary)
	grade = applyExtras(s, grade, &summary)
	grade = applyAttributeScores(s, grade, &summary)
	grade = normalizeGrade(grade)

	summary += formatSummaryFooter(grade)

	exactStr := fmt.Sprintf("%.1f", grade)
	exact, _ := strconv.ParseFloat(exactStr, 64)
	rounded := math.RoundToEven(exact)

	score := ServiceScore{Value: int(rounded), Summary: summary}
	cache.Cache.SetWithTTL(cacheKey, score, 1, 30*time.Minute)
	return score
}

func calculateBaseScore(s *ent.Service, summary *string) float64 {
	score, exists := kycLevelScores[s.KycLevel]
	if !exists {
		score = baseScore
	}
	*summary += fmt.Sprintf("\n|| %-6.2f --> base score for KYC Level %d", score, s.KycLevel)
	return score
}

func applyAttributeScores(s *ent.Service, grade float64, summary *string) float64 {
	isp2p := false
	for _, attr := range s.Edges.Attributes {
		if attr.Bonus != 0 {
			grade += attr.Bonus
			*summary += fmt.Sprintf("\n|| %-6.2f --> %s attribute", attr.Bonus, attr.Title)
		}
		if score, exists := attributeScores[attr.Rating.String()]; exists {
			grade += score
			*summary += fmt.Sprintf("\n|| %-6.2f --> %s attribute", score, attr.Rating)
		}

		if attr.Slug == "peer-to-peer-network" {
			isp2p = true
		}
	}

	if !isp2p && grade >= 9.5 {
		grade -= 0.5
		*summary += fmt.Sprintf("\n|| %-6.2f --> not a peer-to-peer network", -0.5)
	}

	return grade
}

func applyExtras(s *ent.Service, grade float64, summary *string) float64 {
	if s.Xmr {
		grade += xmrBonus
		*summary += fmt.Sprintf("\n|| %-6.2f --> supports Monero", xmrBonus)
	}
	if s.Btc || s.Cash {
		grade += cashBTCBonus
		*summary += fmt.Sprintf("\n|| %-6.2f --> supports Bitcoin or Cash", cashBTCBonus)
	}
	if len(s.OnionUrls) > 0 {
		grade += torBonus
		*summary += fmt.Sprintf("\n|| %-6.2f --> supports Tor", torBonus)
	}
	if s.Verified {
		grade += verifiedBonus
		*summary += fmt.Sprintf("\n|| %-6.2f --> verified", verifiedBonus)
	}
	return grade
}

// func applyTosReviewsPenalty(s *ent.Service, grade float64, summary *string) float64 {
// 	if s.TosReviews == nil {
// 		s.TosReviews = []database.TosReview{}
// 	}

// 	warnings := 0
// 	penalty := 0.0
// 	for _, tr := range s.TosReviews {
// 		if tr.Warning {
// 			penalty -= 0.02
// 			warnings++
// 		}
// 	}

// 	penalty = math.Max(penalty, -3.0)
// 	grade += penalty

// 	if penalty != 0 {
// 		*summary += fmt.Sprintf("\n|| %-6.2f --> %d terms of service warnings", penalty, warnings)
// 	}

// 	return grade
// }

// func hasAttribute(s *ent.Service, attr string) bool {
// 	for _, a := range s.Edges.Attributes {
// 		if a.ID == attr {
// 			return true
// 		}
// 	}
// 	return false
// }

func normalizeGrade(grade float64) float64 {
	return math.Min(maxScore, math.Max(minScore, grade))
}

func formatSummaryFooter(grade float64) string {
	// Force exact decimal using string formatting then parse back
	exactStr := fmt.Sprintf("%.1f", grade)
	exact, _ := strconv.ParseFloat(exactStr, 64)
	rounded := math.RoundToEven(exact)

	return fmt.Sprintf(`
||-----------------------------------------------
|| %d --> Total (rounded to even)
||-----------------------------------------------

Check out the score algorithm here: https://github.com/pluja/kycnot.me
If you think there's a better way to compute score, please, open an issue with your ideas.`, int(rounded))
}
