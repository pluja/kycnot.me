package workers

import "time"

func StartWorkers() {
	// 5 minute ticker
	// ticker5m := time.NewTicker(5 * time.Minute)
	// go func() {
	// 	for range ticker5m.C {
	// 	}
	// }()

	// // 10 minute ticker
	// ticker10m := time.NewTicker(10 * time.Minute)
	// go func() {
	// 	for range ticker10m.C {
	// 	}
	// }()

	// 30 minute ticker
	updateServiceScores()
	ticker30m := time.NewTicker(30 * time.Minute)
	go func() {
		for range ticker30m.C {
			updateServiceScores()
		}
	}()

	// monthly ticker
	tickerMonthly := time.NewTicker(30 * 24 * time.Hour)
	go func() {
		for range tickerMonthly.C {
			UpdateTosReviews()
		}
	}()
}
