package workers

import (
	"context"
	"time"

	"github.com/phuslu/log"

	"kycnot.me/internal/database"
	"kycnot.me/internal/ent/service"
	"kycnot.me/utils/ai"
)

func UpdateTosReviews() {
	serviceList, err := database.Client().Service.Query().
		Where(service.Pending(false)).
		Where(service.Listed(true)).
		Where(service.PotentialScam(false)).WithAttributes().All(context.Background())
	if err != nil {
		log.Error().Err(err).Msg("Error querying services")
		return
	}

	for _, service := range serviceList {
		log.Info().Msgf("Reviewing service %s", service.Name)
		ai.ReviewServiceTos(service)
		time.Sleep(1 * time.Second) // Sleep for 1 second to avoid rate limiting
	}
}
