package workers

import (
	"context"

	"github.com/phuslu/log"
	"kycnot.me/internal/database"
	"kycnot.me/utils"
)

func updateServiceScores() {
	log.Debug().Msgf("Computing scores...")
	ctx := context.Background()

	serviceList, err := database.Client().Service.Query().WithAttributes().All(ctx)
	if err != nil {
		log.Error().Err(err).Msg("Error querying services")
		return
	}

	for _, service := range serviceList {
		if service == nil {
			continue
		}
		sc := utils.ComputeScore(service)
		if _, err := database.Client().Service.UpdateOneID(service.ID).
			SetScore(sc.Value).
			Save(ctx); err != nil {
			log.Error().Err(err).Msgf("Error updating score for service %s", service.Name)
		}
	}
}
