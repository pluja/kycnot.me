package pow

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/dgraph-io/ristretto"
	"github.com/phuslu/log"

	"kycnot.me/utils"
)

var (
	globalChallenger *PowChallenger
	once             sync.Once
)

func init() {
	once.Do(func() {
		var err error
		globalChallenger, err = NewPowChallenger()
		if err != nil {
			log.Fatal().Err(err).Msg("Failed to initialize global PoW challenger")
		}
	})
}

// GetChallenger returns the globally available PowChallenger
func Challenger() *PowChallenger {
	return globalChallenger
}

type PowChallenger struct {
	increaseEveryChallenges int
	baseDifficulty          int
	cache                   *ristretto.Cache
	challengeCount          int64
	challengeExpiration     time.Duration
}

func NewPowChallenger() (*PowChallenger, error) {
	p := &PowChallenger{
		increaseEveryChallenges: 10,
		baseDifficulty:          4,
		challengeExpiration:     1 * time.Minute,
	}

	if err := p.init(); err != nil {
		return nil, err
	}

	return p, nil
}

func (p *PowChallenger) init() error {
	log.Info().Msg("Initializing PoW challenger.")

	if iec, err := strconv.Atoi(utils.Getenv("POW_INCREASE_EVERY_CHALLENGES", "20")); err == nil {
		p.increaseEveryChallenges = iec
	}

	if diff, err := strconv.Atoi(utils.Getenv("POW_DIFFICULTY", "4")); err == nil {
		p.baseDifficulty = diff
	}

	cache, err := ristretto.NewCache(&ristretto.Config{
		NumCounters: 1e7,
		MaxCost:     1 << 30,
		BufferItems: 128,
	})
	if err != nil {
		return fmt.Errorf("failed to create cache: %w", err)
	}
	p.cache = cache

	return nil
}

func (p *PowChallenger) GenerateChallenge(length int) (challenge string, difficulty int, err error) {
	if length < 16 || length > 128 {
		return "", 0, errors.New("invalid challenge length")
	}

	bytes := make([]byte, length/2)
	if _, err := rand.Read(bytes); err != nil {
		return "", 0, fmt.Errorf("failed to generate random bytes: %w", err)
	}

	challenge = hex.EncodeToString(bytes)

	count := atomic.AddInt64(&p.challengeCount, 1)
	difficulty = p.baseDifficulty

	if count%int64(p.increaseEveryChallenges) == 0 && count != 0 {
		difficulty++
	}

	log.Debug().Str("challenge", challenge).Int("difficulty", difficulty).Msg("Generated challenge")

	challengeKey := fmt.Sprintf("pow-%s-d", challenge)

	if !p.cache.SetWithTTL(challengeKey, byte(difficulty), 1, p.challengeExpiration) {
		log.Error().Msgf("Failed to set challenge in cache: %s", challengeKey)
		return "", 0, errors.New("failed to set challenge in cache")
	}

	return challenge, difficulty, nil
}

func (p *PowChallenger) VerifyProof(challenge, nonce string) bool {
	challengeKey := fmt.Sprintf("pow-%s-d", challenge)

	difficultyByte, found := p.cache.Get(challengeKey)
	if !found {
		log.Error().Str("challenge", challenge).Msg("Challenge not found in cache")
		return false
	}

	difficulty := int(difficultyByte.(byte))

	candidate := fmt.Sprintf("%s:%s", challenge, nonce)
	hash := sha256.Sum256([]byte(candidate))
	hashHex := hex.EncodeToString(hash[:])

	result := strings.HasPrefix(hashHex, strings.Repeat("0", difficulty))

	return result
}
