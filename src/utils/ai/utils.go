package ai

import (
	"encoding/json"
	"errors"
	"strings"

	"kycnot.me/internal/ent/schema"
)

func jsonToHighlights(jsonStr string) ([]schema.TosReview, error) {
	jsonStr = strings.TrimPrefix(jsonStr, "```json\n")
	jsonStr = strings.TrimSuffix(jsonStr, "\n```")
	var highlights []schema.TosReview
	// Extract the `analysis` array from the JSON string and unmarshal it into the highlights slice.

	var m map[string]json.RawMessage // use RawMessage for delayed decoding
	err := json.Unmarshal([]byte(jsonStr), &m)
	if err != nil {
		return highlights, err
	}

	tosAnalysis, ok := m["analysis"]
	if !ok {
		return highlights, errors.New("key 'analysis' not found in JSON")
	}

	err = json.Unmarshal(tosAnalysis, &highlights)
	if err != nil {
		return highlights, err
	}

	return highlights, nil
}
