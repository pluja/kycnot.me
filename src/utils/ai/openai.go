package ai

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/phuslu/log"
	"github.com/sashabaranov/go-openai"

	"kycnot.me/internal/database"
	"kycnot.me/internal/ent"
)

func ReviewServiceTos(service *ent.Service) error {
	if service == nil {
		return errors.New("service cannot be nil")
	}

	if len(service.TosUrls) == 0 {
		return errors.New("no TOS URLs found for service")
	}

	resp, err := http.Get("https://r.jina.ai/" + service.TosUrls[0])
	if err != nil {
		log.Error().Err(err).Msg("failed to fetch TOS content")
		return fmt.Errorf("failed to fetch TOS content: %w", err)
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("failed to read response body")
		return fmt.Errorf("failed to read response body: %w", err)
	}

	text := string(body)
	if len(text) < 10 {
		return errors.New("TOS content too short or empty")
	}

	client := openai.NewClient(os.Getenv("OPENAI_API_KEY"))
	ctx := context.Background()

	oaiResp, err := client.CreateChatCompletion(
		ctx,
		openai.ChatCompletionRequest{
			Model:          openai.GPT4o,
			ResponseFormat: &openai.ChatCompletionResponseFormat{Type: openai.ChatCompletionResponseFormatTypeJSONObject},
			Messages: []openai.ChatCompletionMessage{
				{
					Role:    openai.ChatMessageRoleSystem,
					Content: sysPrompt,
				},
				{
					Role:    openai.ChatMessageRoleUser,
					Content: text,
				},
			},
		},
	)
	if err != nil {
		log.Error().Err(err).Msg("OpenAI API request failed")
		return fmt.Errorf("OpenAI API request failed: %w", err)
	}

	if len(oaiResp.Choices) == 0 {
		log.Warn().Msg("no response choices from OpenAI")
		return nil
	}

	highlights, err := jsonToHighlights(oaiResp.Choices[0].Message.Content)
	if err != nil {
		log.Error().Err(err).Msg("failed to parse highlights from response")
		return fmt.Errorf("failed to parse highlights: %w", err)
	}

	log.Debug().Interface("highlights", highlights).Msg("parsed TOS highlights")

	if _, err := database.Client().Service.UpdateOneID(service.ID).
		SetTosReviews(highlights).
		SetLastTosReview(time.Now()).
		Save(ctx); err != nil {
		log.Error().Err(err).Str("service", service.Name).Msg("failed to update service TOS review")
		return fmt.Errorf("failed to update service: %w", err)
	}

	return nil
}
