package ulid

import (
	"math/rand"
	"time"
)

// uniqueULID generates a unique ULID for each record.
func UniqueULID() string {
	t := time.Now()
	entropy := Monotonic(rand.New(rand.NewSource(t.UnixNano())), 0)
	return MustNew(Timestamp(t), entropy).String()
}
