package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/phuslu/log"

	"kycnot.me/internal/cache"
	"kycnot.me/internal/database"
	"kycnot.me/internal/server"
	"kycnot.me/utils/workers"
)

func init() {
	godotenv.Load()
	initLogger(false)
	// loadConfig()
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: dingqr <command> [arguments]")
		os.Exit(1)
	}

	command := os.Args[1]

	switch command {
	case "serve":
		serveCmd := flag.NewFlagSet("serve", flag.ExitOnError)
		devMode := serveCmd.Bool("dev", false, "activate dev mode")
		serveCmd.Parse(os.Args[2:])
		serve(*devMode)
	case "scrap":
		scrapCmd := flag.NewFlagSet("scrap", flag.ExitOnError)
		slugFlag := scrapCmd.String("slug", "", "service slug to scrap")
		scrapCmd.Parse(os.Args[2:])

		initLogger(true)
		cache.Init()
		database.Init()

		if *slugFlag != "" {
			fmt.Printf("Service slug: %s\n", *slugFlag)
			os.Exit(0)
		}
		workers.UpdateTosReviews()
	default:
		fmt.Printf("Unknown command: %s\n", command)
		os.Exit(1)
	}
}

func serve(devMode bool) {
	initLogger(devMode)
	cache.Init()
	database.Init()
	go workers.StartWorkers()
	if err := server.StartServer(); err != nil {
		log.Fatal().Err(err)
	}
}

func initLogger(devMode bool) {
	level := log.InfoLevel
	if devMode {
		level = log.DebugLevel
	}
	if log.IsTerminal(os.Stderr.Fd()) {
		log.DefaultLogger = log.Logger{
			Level:      level,
			TimeField:  "time",
			TimeFormat: "15:04:05",
			Caller:     1,
			Writer: &log.ConsoleWriter{
				ColorOutput:    true,
				QuoteString:    true,
				EndWithMessage: true,
			},
		}
	} else {
		log.DefaultLogger = log.Logger{
			Level:      level,
			Caller:     1,
			TimeField:  "date",
			TimeFormat: "2006-01-02",
			Writer:     &log.IOWriter{os.Stdout},
		}
	}
}
