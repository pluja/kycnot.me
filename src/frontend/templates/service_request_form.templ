package templates

import (
	"kycnot.me/internal/ent"
	"kycnot.me/internal/ent/attribute"
	"kycnot.me/internal/models"
)

func getAttributeColor(t attribute.Rating) string {
	switch t {
	case attribute.RatingBad:
		return "text-red-400/80"
	case attribute.RatingInfo:
		return "text-blue-400/80"
	case attribute.RatingGood:
		return "text-green-400/80"
	case attribute.RatingWarn:
		return "text-yellow-400/80"
	default:
		return ""
	}
}

templ RequestForm(opts *models.BaseOpts, attributes []*ent.Attribute) {
	@Base("Request") {
		if opts.Error != "" {
			<div class="flex justify-center items-center">
				<p class="px-2 py-2 font-bold text-center rounded-lg bg-red-500/70 text-md text-neutral-100">
					{ opts.Error }
				</p>
			</div>
		}
		if opts.Message != "" {
			<div class="flex justify-center items-center">
				<p class="px-2 py-2 font-bold text-center rounded-lg text-md bg-blue-500/70 text-neutral-100">
					{ opts.Message }
				</p>
			</div>
		}
		<section class="flex flex-col items-center py-8 w-full">
			<h2 class="text-2xl font-bold">Request a service</h2>
			<small class="opacity-70">*This form requires JavaScript, <a class="text-lime-600" href="/about#request">read more here</a>.</small>
			<form action="/request" method="POST" class="my-6 space-y-4 max-w-lg font-mono">
				<div class="flex flex-col">
					<label class="font-bold" for="type">Service Type</label>
					<select
						name="type"
						id="type"
						required
						class="p-2 text-white uppercase bg-opacity-30 rounded-lg border bg-zinc-900 border-white/30"
					>
						<option selected value="exchange">exchange</option>
						<option value="service">service</option>
					</select>
				</div>
				<div id="category-container" class="flex flex-col">
					<label class="font-bold" for="category">Service Category</label>
					<select
						name="category"
						id="category"
						required
						class="p-2 text-white uppercase bg-opacity-30 rounded-lg border bg-zinc-900 border-white/30"
					>
						<option value="aggregator">aggregator</option>
						<option value="atm">physical atm</option>
						<option value="cloud">cloud storage</option>
						<option value="email">email</option>
						<option value="goods">goods</option>
						<option value="hosting">hosting / domains</option>
						<option value="market">market</option>
						<option value="proxy store">proxy store</option>
						<option value="sms">sms / phone number</option>
						<option value="tools">tools</option>
						<option value="vpn">vpn</option>
						<option value="ai">ai</option>
						<option value="vps">server hosting (vps)</option>
					</select>
				</div>
				<div class="flex flex-col">
					<label class="font-bold" for="name">Service Name</label>
					<input
						class="p-2 text-white bg-opacity-30 rounded-lg border placeholder-white/30 bg-zinc-900 border-white/30"
						placeholder="Awesome exchange"
						type="text"
						name="name"
						id="name"
						required
					/>
				</div>
				<div class="flex flex-col">
					<label class="font-bold" for="description">Description</label>
					<textarea
						class="p-2 font-mono text-white bg-opacity-30 rounded-lg border placeholder-white/30 bg-zinc-900 border-white/30"
						name="description"
						id="description"
						placeholder="concise description about what the service is. max 500 characters."
						cols="30"
						rows="5"
						required
						minlength="60"
						maxlength="500"
					></textarea>
				</div>
				<div class="flex flex-col">
					<label class="font-bold" for="urls">Service URL(s)</label>
					<small class="text-white/30">*If more than one, separate with commas.</small>
					<input
						class="p-2 text-white bg-opacity-30 rounded-lg border placeholder-white/30 bg-zinc-900 border-white/30"
						type="text"
						name="urls"
						id="urls"
						required
						placeholder="https://example.com"
					/>
				</div>
				<div class="flex flex-col">
					<label class="font-bold" for="logo_url">Logo URL</label>
					<input
						class="p-2 text-white bg-opacity-30 rounded-lg border placeholder-white/30 bg-zinc-900 border-white/30"
						type="text"
						name="logo_url"
						id="logo_url"
						placeholder="https://i.imgur.com/wwwHfCN.jpeg"
						required
					/>
				</div>
				<div class="flex flex-col">
					<label class="font-bold" for="tos_urls">ToS URL(s)</label>
					<small class="text-white/30">*If more than one, separate with commas.</small>
					<input
						class="p-2 text-white bg-opacity-30 rounded-lg border placeholder-white/30 bg-zinc-900 border-white/30"
						type="text"
						name="tos_urls"
						id="tos_urls"
						required
						placeholder="https://example.com/terms"
					/>
				</div>
				<div class="flex flex-col">
					<label class="font-bold" for="onion_urls">Tor Onion URL(s)</label>
					<small class="text-white/30">*If more than one, separate with commas.</small>
					<input
						class="p-2 text-white bg-opacity-30 rounded-lg border placeholder-white/30 bg-zinc-900 border-white/30"
						type="text"
						name="onion_urls"
						id="onion_urls"
						placeholder="http://zbiowkw...onion"
					/>
				</div>
				<div class="flex flex-col">
					<label class="font-bold" for="tags">Keywords</label>
					<small class="text-white/30">*If more than one, separate with commas.</small>
					<input
						class="p-2 text-white bg-opacity-30 rounded-lg border placeholder-white/30 bg-zinc-900 border-white/30"
						type="text"
						name="tags"
						id="tags"
						required
						placeholder="private,p2p, fast"
					/>
				</div>
				<div class="flex flex-col justify-center items-center pt-4 space-x-2">
					<h3 class="mb-2 text-xl">Accepted currencies</h3>
					<div class="flex space-x-4">
						<span class="flex flex-col justify-center items-center">
							<label class="text-xs uppercase" for="btc">BITCOIN</label>
							<input
								class="text-lime-500 bg-opacity-30 rounded-full border-lime-500 appearance-none outline-none bg-zinc-900 focus:ring-0"
								type="checkbox"
								name="btc"
								id="btc"
							/>
						</span>
						<span class="flex flex-col justify-center items-center">
							<label class="text-xs uppercase" for="xmr">MONERO</label>
							<input
								class="text-lime-500 bg-opacity-30 rounded-full border-lime-500 appearance-none outline-none bg-zinc-900 focus:ring-0"
								type="checkbox"
								name="xmr"
								id="xmr"
							/>
						</span>
						<span class="flex flex-col justify-center items-center">
							<label class="text-xs uppercase" for="ln">LIGHTNING</label>
							<input
								class="text-lime-500 bg-opacity-30 rounded-full border-lime-500 appearance-none outline-none bg-zinc-900 focus:ring-0"
								type="checkbox"
								name="ln"
								id="ln"
							/>
						</span>
						<span class="flex flex-col justify-center items-center">
							<label class="text-xs uppercase" for="fiat">FIAT</label>
							<input
								class="text-lime-500 bg-opacity-30 rounded-full border-lime-500 appearance-none outline-none bg-zinc-900 focus:ring-0"
								type="checkbox"
								name="fiat"
								id="fiat"
							/>
						</span>
						<span class="flex flex-col justify-center items-center">
							<label class="text-xs uppercase" for="cash">CASH</label>
							<input
								class="text-lime-500 bg-opacity-30 rounded-full border-lime-500 appearance-none outline-none bg-zinc-900 focus:ring-0"
								type="checkbox"
								name="cash"
								id="cash"
							/>
						</span>
					</div>
				</div>
				<div class="space-y-4">
					<h3 class="font-bold">KYC Level</h3>
					<label class="flex flex-row items-center p-2 rounded-lg border border-white/10" for="kyclevel0">
						<input type="radio" name="kyc_level" id="kyclevel0" value="0"/>
						<p class="ml-3">
							<b class="text-lime-400">Level 0</b>: No KYC ever. The ToS do not mention nor enforce
							any KYC/AML verification.
						</p>
					</label>
					<label class="flex flex-row items-center p-2 rounded-lg border border-white/10" for="kyclevel1">
						<input type="radio" name="kyc_level" id="kyclevel1" value="1"/>
						<p class="ml-3">
							<b class="text-lime-400">Level 1</b>: No KYC/AML procedures are mentioned as such,
							however the service reserves the right to share acquired data with authorities, block funds or
							reject transactions.
						</p>
					</label>
					<label class="flex flex-row items-center p-2 rounded-lg border border-white/10" for="kyclevel2">
						<input type="radio" name="kyc_level" id="kyclevel2" value="2" checked/>
						<p class="ml-3">
							<b class="text-lime-400">Level 2</b>: KYC is not mandatory, but the service can require
							KYC from any user at any time, and it may block funds. This could occur, for example, if a
							transaction is flagged suspicious.
						</p>
					</label>
					<label class="flex flex-row items-center p-2 rounded-lg border border-white/10" for="kyclevel3">
						<input type="radio" name="kyc_level" id="kyclevel3" value="3"/>
						<p class="ml-3">
							<b class="text-lime-400">Level 3</b>: KYC is mandatory to use certain features of the
							platform. Non-KYC users can be asked to verify their identity at any time, for any reason and may
							have their service restricted or revoked.
						</p>
					</label>
				</div>
				<h3 class="font-bold">Attributes</h3>
				<small class="text-white/50">* Scrollable. You must check at least 3</small>
				<div class="overflow-scroll p-4 mx-4 mb-4 space-y-2 max-w-lg h-52 rounded-lg border border-white/10">
					for _, attr := range attributes {
						<span class="block p-1 border-b border-white/10">
							<input type="checkbox" id={ attr.ID } name="attributes" value={ attr.ID }/>
							<label class={ getAttributeColor(attr.Rating) + " ml-1" } for={ attr.ID }>{ attr.Title }</label>
						</span>
					}
				</div>
				<noscript>
					<p class="my-2 font-bold text-yellow-500 uppercase">
						You need to enable JavaScript to complete this form.
					</p>
				</noscript>
				<div
					id="start-pow"
					class="flex justify-center items-center px-4 py-2 mt-2 mb-4 space-x-2 font-bold uppercase bg-blue-900 rounded-lg cursor-pointer"
				>
					<input type="text" name="pow-nonce" id="pow-nonce" hidden required value=""/>
					<input type="text" name="pow-id" id="pow-id" hidden required value=""/>
					<svg
						id="pow-spinner"
						class="hidden text-white fill-white"
						width="24"
						height="24"
						viewBox="0 0 24 24"
						xmlns="http://www.w3.org/2000/svg"
					>
						<style>
                            .spinner_ZCsl {
                                animation: spinner_qV4G 1.2s cubic-bezier(0.52, .6, .25, .99) infinite
                            }

                            .spinner_gaIW {
                                animation-delay: .6s
                            }

                            @keyframes spinner_qV4G {
                                0% {
                                    r: 0;
                                    opacity: 1
                                }

                                100% {
                                    r: 11px;
                                    opacity: 0
                                }
                            }
                        </style>
						<circle class="spinner_ZCsl" cx="12" cy="12" r="0"></circle>
						<circle class="spinner_ZCsl spinner_gaIW" cx="12" cy="12" r="0"></circle>
					</svg>
					<span id="pow-text">🧠 I'm Human</span>
				</div>
				<button
					id="submit-btn"
					class="hidden px-4 py-2 my-4 space-x-2 w-full font-bold uppercase bg-lime-900 rounded-lg cursor-pointer"
				>
					Submit
				</button>
			</form>
		</section>
		<script src="/static/js/pow.js"></script>
		<script>
            // Toggle category select based on type select
            document.addEventListener('DOMContentLoaded', (event) => {
                const typeSelect = document.getElementById('type');
                const categoryDiv = document.getElementById('category-container');

                function toggleCategory() {
                    if (typeSelect.value === 'service') {
                        categoryDiv.style.display = 'flex'; // Show
                    } else {
                        categoryDiv.style.display = 'none'; // Hide
                    }
                }
                // Initial toggle based on the preselected option
                toggleCategory();
                // Add change event listener to the type select element
                typeSelect.addEventListener('change', toggleCategory);
            });
        </script>
	}
}
