/** @type {import('tailwindcss').Config} */
const { addDynamicIconSelectors } = require('@iconify/tailwind');

module.exports = {
  content: ['./templates/**/*.{html,templ,js}', './static/js/*.js'],
  theme: {
    extend: {
      fontFamily: {
        samo: ['samo', 'monospace'],
        inter: ['inter', 'sans-serif'],
      },
      colors: {
        background: 'var(--kycnot-background)',
        primary: 'var(--kycnot-primary)',
        'primary-emphasis': 'var(--kycnot-primary-emphasis)',
        'primary-dark': 'var(--kycnot-primary-dark)',
        'primary-light': 'var(--kycnot-primary-light)',
        'kycnot-text': 'var(--kycnot-text)',
        'kycnot-text-fade': 'var(--kycnot-text-fade)',
        'kycnot-text-select': 'var(--kycnot-text-select)',
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
    addDynamicIconSelectors(),
  ],
};
