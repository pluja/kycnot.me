const POW = {
  challenge: null,
  difficulty: null,
  lastNonce: null,

  async fetchChallenge() {
    const response = await fetch('/api/pow')
    if (!response.ok) throw new Error('Failed to fetch challenge')
    const data = await response.json()
    if (data.error) throw new Error(data.error)
    this.challenge = data.challenge
    this.difficulty = data.difficulty
  },

  async computeProof(challenge, difficulty) {
    const leadingZeros = '0'.repeat(difficulty)
    const encoder = new TextEncoder()
    let nonce = 0
    while (true) {
      const candidate = `${challenge}:${nonce}`
      const hash = encoder.encode(candidate)
      const hashArrayBuffer = await crypto.subtle.digest('SHA-256', hash)
      const hashHex = Array.from(new Uint8Array(hashArrayBuffer))
        .map((b) => b.toString(16).padStart(2, '0'))
        .join('')
      if (hashHex.startsWith(leadingZeros)) {
        console.log(hashHex)
        return nonce.toString()
      }
      nonce++
      if (nonce % 1000 === 0) {
        await new Promise((resolve) => setTimeout(resolve, 0))
      }
    }
  },

  async verifyProof(challenge, nonce) {
    const url = `/api/pow/verify/${encodeURIComponent(challenge)}/${encodeURIComponent(nonce)}`
    const response = await fetch(url, { method: 'GET' })
    if (!response.ok) throw new Error('Network response was not ok')
    return response.json()
  },

  updateUI(element, state) {
    const spinner = document.querySelector('#pow-spinner')
    const text = document.querySelector('#pow-text')
    const submit = document.querySelector('#submit-btn')
    const nonceInput = document.querySelector('#pow-nonce')
    const idInput = document.querySelector('#pow-id')
    element.className = 'pow-button'
    spinner.classList.toggle('hidden', state !== 'computing')
    element.classList.add(
      state === 'computing'
        ? 'bg-gray-700'
        : state === 'verified'
          ? 'bg-lime-700'
          : state === 'failed'
            ? 'bg-red-700'
            : 'bg-blue-900'
    )
    if (state === 'verified') {
      element.classList.add('disabled')
      nonceInput.value = this.lastNonce
      idInput.value = this.challenge
    }
    element.classList.toggle('cursor-pointer', state !== 'verified')
    text.textContent =
      state === 'computing'
        ? 'Computing...'
        : state === 'verified'
          ? '✅ Verified!'
          : state === 'failed'
            ? '❌ Failed!'
            : 'An error occurred!'
    submit.classList.toggle('hidden', state !== 'verified')
  },

  async startPow() {
    const element = document.querySelector('#start-pow')
    if (!element) throw new Error('Start POW element not found')
    this.updateUI(element, 'computing')
    try {
      await this.fetchChallenge()
      this.lastNonce = await this.computeProof(this.challenge, this.difficulty)
      const response = await this.verifyProof(this.challenge, this.lastNonce)
      console.log(response)
      const valid = response.status === 200 && response.valid === true
      this.updateUI(element, valid ? 'verified' : 'failed')
    } catch (error) {
      console.error('Error:', error)
      this.updateUI(element, 'error')
    }
  },

  init() {
    const element = document.querySelector('#start-pow')
    if (element) {
      element.addEventListener('click', () => this.startPow())
    }
  },
}

document.addEventListener('DOMContentLoaded', () => POW.init())
