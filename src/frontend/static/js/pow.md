## Usage

### Go

```go
// Create a new PoW challenger
powChallenger := pow.NewPowChallenger()
powChallenger.Difficulty = 4

// Create a new server

// Add PoW challenger to server

// Start server

// Form page handler
func (s *Server) handleGetFormPage(c *fiber.Ctx) error {
    // Create a new PoW challenge with 16 char challenge
 challenge, id, difficulty, err := s.PowChallenger.PowGenerateChallenge(16)
 if err != nil {
  return err
 }

 reverse := true
 return c.Render("my_form", fiber.Map{
  "Title":   "Some Form",
  "Pow": fiber.Map{
   "Challenge":  challenge,
   "Difficulty": difficulty,
   "Id":         id,
  },
 })
}
```

### HTML

```html
<form action="" method="POST">
    <noscript>
        <p class="my-2 font-bold text-yellow-500 uppercase">
            You need to enable JavaScript to complete this form.
        </p>
    </noscript>
    <div id="start-pow" class="flex items-center justify-center px-4 py-2 mt-2 mb-4 space-x-2 font-bold uppercase bg-blue-900 rounded-lg cursor-pointer" data-pow="{{.Pow.Id}}" data-pow-c="{{.Pow.Challenge}}" data-pow-d="{{.Pow.Difficulty}}">
        <input type="text" name="pow-nonce" id="pow-nonce" hidden required value="">
        <input type="text" name="pow-id" id="pow-id" hidden required value="{{.Pow.Id}}">
        <svg id="pow-spinner" class="hidden text-white fill-white" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><style>.spinner_ZCsl{animation:spinner_qV4G 1.2s cubic-bezier(0.52,.6,.25,.99) infinite}.spinner_gaIW{animation-delay:.6s}@keyframes spinner_qV4G{0%{r:0;opacity:1}100%{r:11px;opacity:0}}</style><circle class="spinner_ZCsl" cx="12" cy="12" r="0"/><circle class="spinner_ZCsl spinner_gaIW" cx="12" cy="12" r="0"/></svg>
        <span id="pow-text">🧠 I'm Human</span>
    </div>
    <button id="submit-btn" class="hidden w-full px-4 py-2 my-4 space-x-2 font-bold uppercase rounded-lg cursor-pointer bg-lime-900">
        Submit
    </button>
</form>


<script src="/static/js/pow.js"></script>
<script>
    // Initialize PoW service
    startPow();
</script>
```

### Go

```go
func (s *Server) FormHandler(w http.ResponseWriter, r *http.Request) {
    // Get PoW ID and nonce from request
    id := r.FormValue("pow-id")
    nonce := r.FormValue("pow-nonce")

    // Validate PoW
    if err := s.PowChallenger.PowValidate(id, nonce); err != nil {
        // Handle error, pow is invalid
    }

    // PoW is valid
}
```
