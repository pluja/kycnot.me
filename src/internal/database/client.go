// client.go
package database

import (
	"context"
	"os"

	"entgo.io/ent/dialect"
	_ "github.com/go-sql-driver/mysql"
	"github.com/phuslu/log"

	"kycnot.me/internal/ent"
	"kycnot.me/utils"
)

var client *ent.Client

// Init initializes the database client.
func Init() {
	var err error
	// client, err = ent.Open(dialect.SQLite, "file:ent?mode=memory&cache=shared&_fk=1")
	// client, err = ent.Open(dialect.SQLite, "file:./db.sql?cache=shared&_fk=1")
	log.Printf("%s", os.Getenv("DATABASE_URI"))
	client, err = ent.Open(dialect.MySQL, utils.Getenv("DATABASE_URI", "kycnot:kycnot@tcp(database:3306)/kycnot?parseTime=True"))
	if err != nil {
		log.Fatal().Msgf("failed opening connection to MYSQL: %v", err)
	}

	// Run the auto migration tool.
	if err := client.Schema.Create(context.Background()); err != nil {
		log.Fatal().Msgf("failed creating schema resources: %v", err)
	}
}

func Client() *ent.Client {
	return client
}
