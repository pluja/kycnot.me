package server

import (
	"os"
	"path"

	"github.com/kataras/iris/v12"
	"github.com/phuslu/log"
	"github.com/samber/lo"
	"kycnot.me/internal/server/handlers"
	"kycnot.me/utils"
)

func StartServer() error {
	r := iris.New()

	r.Use(iris.Compression)

	serverAddress := utils.Getenv("SERVER_ADDRESS", ":1337")

	rootDir := utils.Getenv("ROOT_DIR", "./")
	r.Favicon(path.Join(rootDir, "/frontend/static", "/assets/favicon.webp"))
	r.HandleDir("/static", iris.Dir(path.Join(rootDir, "/frontend/static")), iris.DirOptions{Compress: true})

	r.Get("/", handlers.IndexHandler)
	r.Get("/about", handlers.About)
	r.Get("/changelog", handlers.Changelog)
	r.Get("/request", handlers.GetServiceForm)
	r.Post("/request", handlers.PostServiceForm)

	r.Get("/pending", handlers.ServicePendingList)
	r.Get("/service/{slug:string}", handlers.Service)
	r.Get("/service/{slug:string}/summary", handlers.ServiceSummary)
	r.Get("/service/{slug:string}/proof", handlers.DummyRespond)
	r.Get("/service/{slug:string}/tor", handlers.DummyRespond)
	r.Get("/service/{slug:string}/web", handlers.DummyRespond)

	r.Get("/attribute/{slug:string}", handlers.Attribute)
	r.Get("/attributes", handlers.AttributeList)
	r.Get("/attr/{slug:string}", func(c iris.Context) {
		slug := c.Params().Get("slug")
		c.Redirect("/attribute/"+slug, iris.StatusMovedPermanently)
	})

	r.Get("/api/pow", handlers.GetPow)
	r.Get("/api/pow/verify/{id:string}/{nonce:int}", handlers.VerifyPow)

	// Get / Generate API key
	apiKey := os.Getenv("API_KEY")
	if apiKey == "" {
		apiKey = lo.RandomString(16, lo.LettersCharset)
		log.Info().Msgf("Temporary API Key: %s", apiKey)
	}

	// API zone
	management := r.Party("/api/v1", apiKeyAuth(apiKey))
	management.Use(apiKeyAuth(apiKey))
	{
		management.Post("attribute", handlers.ApiAttributePost)
		management.Post("service", handlers.ApiServicePost)
		management.Post("change", handlers.ApiChangePost)
		management.Patch("service/attribute", handlers.ApiPatchAddAttribute)
		management.Patch("attribute", handlers.DummyRespond)
		management.Patch("change", handlers.DummyRespond)
		management.Patch("service", handlers.DummyRespond)
		management.Get("attribute", handlers.DummyRespond)
		management.Get("service", handlers.DummyRespond)
		management.Get("change", handlers.DummyRespond)
		management.Delete("change", handlers.DummyRespond)
	}

	log.Info().Msgf("Starting server at http://127.0.0.1%s", serverAddress)
	return r.Listen(serverAddress)
}

func apiKeyAuth(expectedKey string) iris.Handler {
	return func(ctx iris.Context) {
		if expectedKey == "" {
			ctx.StatusCode(iris.StatusForbidden)
			ctx.StopExecution()
			return
		}
		apiKey := ctx.GetHeader("X-API-KEY")
		if apiKey != expectedKey {
			ctx.StatusCode(iris.StatusUnauthorized)
			ctx.StopExecution()
			return
		}
		ctx.Next()
	}
}
