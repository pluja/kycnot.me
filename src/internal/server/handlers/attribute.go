package handlers

import (
	"context"
	"fmt"

	slugger "github.com/gosimple/slug"
	"github.com/kataras/iris/v12"
	"github.com/phuslu/log"

	"kycnot.me/frontend/templates"
	"kycnot.me/internal/database"
	"kycnot.me/internal/ent"
	"kycnot.me/internal/ent/attribute"
	"kycnot.me/internal/ent/service"
)

func Attribute(c iris.Context) {
	slug := c.Params().Get("slug")
	if !slugger.IsSlug(slug) {
		c.Redirect(fmt.Sprintf("/attribute/%s", slugger.Make(slug)))
	}

	// cacheKey := slug + "-attr"

	aa, err := database.Client().Attribute.Query().
		Where(attribute.SlugEQ(slug)).
		Order(ent.Asc(attribute.FieldRating)).
		WithServices(func(sq *ent.ServiceQuery) {
			sq.Where(service.PendingEQ(false))
		}).
		First(context.Background())
	if err != nil {
		// Handle the error (e.g., service not found)
		log.Debug().Err(err).Msgf("error")
		c.Redirect("/?error=Attribute not found", iris.StatusSeeOther)
		return
	}

	// Cache service rendered component for 30 minutes
	component := templates.Attribute(aa)
	// cache.Cache.SetWithTTL(cacheKey, component, 1, 30*time.Minute)
	c.RenderComponent(component)
}
