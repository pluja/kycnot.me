package handlers

import (
	"context"

	"github.com/kataras/iris/v12"
	"github.com/phuslu/log"
	"kycnot.me/frontend/templates"
	"kycnot.me/internal/database"
	"kycnot.me/internal/ent"
	"kycnot.me/internal/ent/change"
)

func Changelog(c iris.Context) {
	changes, err := database.Client().Change.Query().
		Order(ent.Desc(change.FieldCreatedAt)).
		WithService().
		All(context.Background())
	if err != nil {
		log.Error().Err(err).Msg("Failed to get changelog")
		if ent.IsNotFound(err) {
			c.StatusCode(iris.StatusNotFound)
			return
		}

		c.StatusCode(iris.StatusInternalServerError)
		return
	}

	c.RenderComponent(templates.Changelog(changes))
}
