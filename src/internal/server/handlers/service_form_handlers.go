package handlers

import (
	"context"
	"fmt"
	"net/url"
	"regexp"
	"sort"
	"strings"

	slugger "github.com/gosimple/slug"
	"github.com/kataras/iris/v12"
	"github.com/microcosm-cc/bluemonday"
	"github.com/phuslu/log"
	"github.com/samber/lo"

	"kycnot.me/frontend/templates"
	"kycnot.me/internal/database"
	"kycnot.me/internal/ent"
	"kycnot.me/internal/ent/service"
	"kycnot.me/internal/models"
	"kycnot.me/utils"
	"kycnot.me/utils/pow"
)

// GET REQUEST
func GetServiceForm(c iris.Context) {
	attributes, err := database.Client().Attribute.Query().All(context.Background())
	if err != nil {
		log.Error().Msgf("%v", err)
	}

	// Sort attributes by Rating
	sort.Slice(attributes, func(i, j int) bool {
		return utils.SortHelperAttributeRating(attributes[i].Rating) < utils.SortHelperAttributeRating(attributes[j].Rating)
	})

	c.RenderComponent(templates.RequestForm(&models.BaseOpts{Error: c.URLParam("error"), Message: c.URLParam("msg")}, attributes))
}

type servicePostForm struct {
	Type        string   `form:"type"`
	Category    string   `form:"category"`
	Name        string   `form:"name"`
	Description string   `form:"description"`
	Urls        string   `form:"urls"`
	LogoUrl     string   `form:"logo_url"`
	TosUrls     string   `form:"tos_urls"`
	OnionUrls   string   `form:"onion_urls"`
	Tags        string   `form:"tags"`
	Xmr         bool     `form:"xmr"`
	Btc         bool     `form:"btc"`
	Ln          bool     `form:"ln"`
	Fiat        bool     `form:"fiat"`
	Cash        bool     `form:"cash"`
	KYCLevel    int      `form:"kyc_level"`
	Attributes  []string `form:"attributes"`
	PowNonce    string   `form:"pow-nonce"`
	PowId       string   `form:"pow-id"`
}

// POST REQUEST
func PostServiceForm(c iris.Context) {
	var data servicePostForm
	if err := c.ReadForm(&data); err != nil {
		log.Debug().Err(err).Msg("Could not parse form data")
		c.Redirect("/request?error=Invalid%20Form", iris.StatusSeeOther)
		return
	}

	log.Debug().Msgf("Nonce: %v, ID: %v", data.PowNonce, data.PowId)
	if !pow.Challenger().VerifyProof(data.PowId, data.PowNonce) {
		log.Debug().Msg("Invalid PoW")
		c.Redirect("/request?error=Invalid%20Captcha", iris.StatusSeeOther)
		return
	}

	if len(data.Attributes) < 1 {
		log.Debug().Msgf("Invalid number of attributes: %v", len(data.Attributes))
		c.Redirect("/request?error=You%20must%20select%20at%20least%203%20attributes", iris.StatusSeeOther)
		return
	}

	// Tags
	tags := strings.FieldsFunc(strings.ToLower(data.Tags), func(c rune) bool {
		return c == ',' || c == ' '
	})
	ts := strings.Join(lo.Uniq(tags), ",")

	// Sanitize
	reg := regexp.MustCompile(`[^a-zA-Zà-ÿ0-9,-]`)
	ts = reg.ReplaceAllString(ts, "")

	reg = regexp.MustCompile(`[^a-zA-Zà-ÿ0-9,\-\s._]`)
	data.Name = reg.ReplaceAllString(data.Name, "")

	// Sanitize against XSS
	descriptionSanitizer := bluemonday.UGCPolicy()
	data.Description = descriptionSanitizer.Sanitize(data.Description)

	slug := slugger.Make(data.Name)

	// Avoid repeated services, check (case insensitive) if a service
	// with that name already exists in database.
	_, err := database.Client().Service.Query().
		Where(service.SlugEQ(slug)).
		Order(ent.Asc(service.FieldID)).
		WithAttributes().
		First(context.Background())

	if err == nil {
		log.Debug().Err(err).Msg("A service with that name already exists")
		c.Redirect(fmt.Sprintf("/request?error=%s", url.QueryEscape("A service with that name already exists")), iris.StatusSeeOther)
		return
	}

	// Set type
	var ty service.Type
	if data.Type == service.TypeExchange.String() {
		ty = service.TypeExchange
	} else {
		ty = service.TypeService
	}

	_, err = database.Client().Service.Create().
		SetTags(strings.Split(ts, ",")).
		SetName(data.Name).
		SetBtc(data.Btc).
		SetXmr(data.Xmr).
		SetLightning(data.Ln).
		SetFiat(data.Fiat).
		SetPending(true).
		SetListed(false).
		SetPotentialScam(false).
		SetCategory(data.Category).
		SetType(ty).
		SetUrls(utils.UrlListParser(data.Urls)).
		SetLogoURL(utils.UrlParser(data.LogoUrl)).
		SetTosUrls(utils.UrlListParser(data.TosUrls)).
		SetOnionUrls(utils.UrlListParser(data.OnionUrls)).
		SetDescription(data.Description).
		SetSlug(slug).
		AddAttributeIDs(data.Attributes...).
		Save(context.Background())
	if err != nil {
		log.Debug().Err(err).Msg("Could not save service to database")
		c.Redirect(fmt.Sprintf("/request?error=%s", url.QueryEscape(err.Error())), iris.StatusSeeOther)
		return
	}

	c.Redirect(fmt.Sprintf("/service/%s", slug), iris.StatusSeeOther)
}
