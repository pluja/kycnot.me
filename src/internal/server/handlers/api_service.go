package handlers

import (
	"context"
	"net/http"
	"time"

	"github.com/gosimple/slug"
	"github.com/kataras/iris/v12"
	"github.com/phuslu/log"

	"kycnot.me/internal/database"
	"kycnot.me/internal/ent/attribute"
	"kycnot.me/internal/ent/schema"
	"kycnot.me/internal/ent/service"
)

type ServiceCreateRequest struct {
	Name        string                 `json:"name"`
	Slug        string                 `json:"slug"`
	Description string                 `json:"description"`
	LogoURL     string                 `json:"logo_url,omitempty"`
	TosURLs     []string               `json:"tos_urls,omitempty"`
	OnionURLs   []string               `json:"onion_urls,omitempty"`
	Category    string                 `json:"category,omitempty"`
	Comments    []string               `json:"comments,omitempty"`
	TosReviews  []schema.TosReview     `json:"tos_reviews,omitempty"`
	URLs        []string               `json:"urls"`
	Tags        []string               `json:"tags,omitempty"`
	KYCLevel    int                    `json:"kyc_level,omitempty"`
	Type        string                 `json:"type,omitempty"`
	Support     []schema.SupportMethod `json:"support,omitempty"`
	Listed      bool                   `json:"listed,omitempty"`
	Verified    bool                   `json:"verified,omitempty"`
	Xmr         bool                   `json:"xmr,omitempty"`
	Btc         bool                   `json:"btc,omitempty"`
	Lightning   bool                   `json:"lightning,omitempty"`
	Cash        bool                   `json:"cash,omitempty"`
	Fiat        bool                   `json:"fiat,omitempty"`
	Pending     bool                   `json:"pending,omitempty"`
	Referral    string                 `json:"referral,omitempty"`
	CreatedAt   time.Time              `json:"created"`
}

func ApiServicePost(c iris.Context) {
	var req ServiceCreateRequest
	if err := c.ReadJSON(&req); err != nil {
		c.StatusCode(http.StatusBadRequest)
		log.Error().AnErr("error", err).Str("service", req.Name)
		c.JSON(iris.Map{"error": "Invalid request body: " + err.Error()})
		return
	}

	svc, err := database.Client().Service.Create().
		SetName(req.Name).
		SetSlug(slug.Make(req.Name)).
		SetDescription(req.Description).
		SetNillableLogoURL(&req.LogoURL).
		SetTosUrls(req.TosURLs).
		SetOnionUrls(req.OnionURLs).
		SetNillableCategory(&req.Category).
		SetComments(req.Comments).
		SetTosReviews(req.TosReviews).
		SetUrls(req.URLs).
		SetTags(req.Tags).
		SetKycLevel(req.KYCLevel).
		SetType(service.Type(req.Type)).
		SetSupport(req.Support).
		SetListed(req.Listed).
		SetVerified(req.Verified).
		SetXmr(req.Xmr).
		SetBtc(req.Btc).
		SetLightning(req.Lightning).
		SetCash(req.Cash).
		SetFiat(req.Fiat).
		SetPending(req.Pending).
		SetReferral(req.Referral).
		SetCreatedAt(req.CreatedAt).
		Save(context.Background())
	if err != nil {
		c.StatusCode(http.StatusInternalServerError)
		log.Error().AnErr("error", err).Str("service", req.Name)
		c.JSON(iris.Map{"error": err.Error()})
		return
	}

	c.StatusCode(http.StatusCreated)
	c.JSON(svc)
}

func ApiPatchAddAttribute(c iris.Context) {
	serviceSlug := slug.Make(c.URLParam("service"))
	attributeSlug := slug.Make(c.URLParam("attribute"))

	svc, err := database.Client().Service.Query().Where(service.SlugEQ(serviceSlug)).Only(context.Background())
	if err != nil {
		c.StatusCode(http.StatusNotFound)
		log.Error().AnErr("error", err).Str("service", serviceSlug)
		c.JSON(iris.Map{"error": "Service not found"})
		return
	}

	attr, err := database.Client().Attribute.Query().Where(attribute.SlugEQ(attributeSlug)).Only(context.Background())
	if err != nil {
		c.StatusCode(http.StatusNotFound)
		log.Error().AnErr("error", err).Str("service", serviceSlug)
		c.JSON(iris.Map{"error": "Attribute not found"})
		return
	}

	_, err = svc.Update().AddAttributes(attr).Save(context.Background())
	if err != nil {
		c.StatusCode(http.StatusInternalServerError)
		log.Error().AnErr("error", err).Str("service", serviceSlug)
		c.JSON(iris.Map{"error": err.Error()})
		return
	}

	c.StatusCode(http.StatusOK)
}
