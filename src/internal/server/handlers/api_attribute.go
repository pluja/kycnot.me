package handlers

import (
	"net/http"

	"github.com/gosimple/slug"
	"github.com/kataras/iris/v12"
	"github.com/phuslu/log"

	"kycnot.me/internal/database"
	"kycnot.me/internal/ent"
	"kycnot.me/internal/ent/attribute"
)

type CreateAttributeRequest struct {
	Title       string           `json:"title"`
	Slug        string           `json:"slug"`
	Rating      attribute.Rating `json:"rating,omitempty"`
	Description string           `json:"description,omitempty"`
}

func ApiAttributePost(c iris.Context) {
	var req CreateAttributeRequest
	if err := c.ReadJSON(&req); err != nil {
		log.Error().Err(err).Msg("failed to read request body")
		c.StopWithError(http.StatusBadRequest, err)
		return
	}

	req.Slug = slug.Make(req.Title)

	attr, err := database.Client().Attribute.Create().
		SetTitle(req.Title).
		SetSlug(req.Slug).
		SetRating(req.Rating).
		SetNillableDescription(String(req.Description)).
		Save(c.Request().Context())
	if err != nil {
		log.Error().Err(err).Msg("failed to create attribute")
		if ent.IsConstraintError(err) {
			c.StopWithError(http.StatusConflict, err)
			return
		}
		c.StopWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(attr)
}

// Helper functions to handle optional fields
func Rating(r attribute.Rating) *attribute.Rating {
	if r == "" {
		return nil
	}
	return &r
}

func String(s string) *string {
	if s == "" {
		return nil
	}
	return &s
}
