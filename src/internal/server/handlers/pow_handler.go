package handlers

import (
	"github.com/kataras/iris/v12"

	"kycnot.me/utils/pow"
)

func VerifyPow(c iris.Context) {
	// Get id, nonce and proof from the request
	id := c.Params().Get("id") // The id is the challenge itself
	nonce := c.Params().Get("nonce")

	// Verify the proof of work
	valid := pow.Challenger().VerifyProof(id, nonce)

	// Return the result
	c.JSON(iris.Map{
		"valid":  valid,
		"status": iris.StatusOK,
	})
}

func GetPow(c iris.Context) {
	// Verify the proof of work
	challenge, difficulty, err := pow.Challenger().GenerateChallenge(16)

	// Prepare the response
	response := iris.Map{
		"challenge":  challenge,
		"difficulty": difficulty,
		"status":     iris.StatusOK,
	}

	// Add error to the response only if it's not nil
	if err != nil {
		response["error"] = err.Error()
		response["status"] = iris.StatusInternalServerError
	}

	// Return the result
	c.JSON(response)
}
