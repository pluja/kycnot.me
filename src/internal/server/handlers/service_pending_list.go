package handlers

import (
	"context"

	"github.com/kataras/iris/v12"
	"github.com/phuslu/log"

	"kycnot.me/frontend/templates"
	"kycnot.me/internal/database"
	"kycnot.me/internal/ent/service"
)

func ServicePendingList(c iris.Context) {
	pendingList, err := database.Client().Service.Query().
		Where(service.Pending(true)).
		Where(service.PotentialScam(false)).
		All(context.Background())
	if err != nil {
		log.Error().Err(err).Msg("Error getting pending services")
	}

	c.RenderComponent(templates.ServicePendingList(pendingList))
}
