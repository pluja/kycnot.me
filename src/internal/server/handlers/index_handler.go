package handlers

import (
	"context"
	"strings"

	"entgo.io/ent/dialect/sql"
	"github.com/kataras/iris/v12"
	"github.com/phuslu/log"

	"kycnot.me/frontend/templates"
	"kycnot.me/internal/database"
	"kycnot.me/internal/ent"
	"kycnot.me/internal/ent/announcement"
	"kycnot.me/internal/ent/predicate"
	"kycnot.me/internal/ent/service"
	"kycnot.me/internal/models"
)

const (
	statusBadRequest = iris.StatusBadRequest
	paramOn          = "on"
)

var supportedCurrencies = []string{"xmr", "btc", "ln", "cash", "fiat"}

func IndexHandler(c iris.Context) {
	ctx := context.Background()
	filters := getFilters(c)

	ann, _ := getActiveAnnouncement(ctx)

	query := buildServiceQuery(filters)
	services, err := query.All(ctx)
	if err != nil {
		log.Printf("Error fetching services: %v", err)
		c.StopWithStatus(statusBadRequest)
		return
	}

	c.RenderComponent(templates.Index(ann, services, filters))
}

func getFilters(c iris.Context) models.ServiceFilters {
	filters := models.ServiceFilters{
		Currencies: make(map[string]bool),
		Type:       c.URLParam("t"),
		Query:      c.URLParam("q"),
	}

	for _, currency := range supportedCurrencies {
		filters.Currencies[currency] = c.URLParamDefault(currency, "off") == paramOn
	}

	return filters
}

func getActiveAnnouncement(ctx context.Context) (*ent.Announcement, error) {
	return database.Client().Announcement.Query().
		Where(announcement.Active(true)).
		Only(ctx)
}

func buildServiceQuery(filters models.ServiceFilters) *ent.ServiceQuery {
	// Base query
	query := database.Client().Service.Query().
		Where(service.Pending(false)).
		Where(service.Listed(true)).
		Where(service.PotentialScam(false)).
		Order(
			service.ByScore(sql.OrderDesc()),
			sql.OrderByRand(),
		).
		WithAttributes()

	if filters.Type != "" {
		log.Debug().Msgf("Apply type filter: %s", filters.Type)
		query.Where(service.TypeEQ(service.Type(filters.Type)))
	}

	if filters.Query != "" {
		log.Debug().Msgf("Apply query filter: %s", filters.Query)
		query.Where(buildSearchPredicate(filters.Query))
	}

	for currency, isSelected := range filters.Currencies {
		if isSelected {
			log.Debug().Msgf("Apply %s filter", currency)
			query.Where(getCurrencyPredicate(currency))
		}
	}

	return query
}

func buildSearchPredicate(q string) predicate.Service {
	q = strings.ToLower(q)
	return predicate.Service(func(s *sql.Selector) {
		s.Where(sql.Or(
			sql.Contains(sql.Lower(s.C(service.FieldDescription)), q),
			sql.Contains(sql.Lower(s.C(service.FieldName)), q),
			sql.Contains(sql.Lower(s.C(service.FieldTags)), q),
			sql.Contains(sql.Lower(s.C(service.FieldUrls)), q),
		))
	})
}

func getCurrencyPredicate(currency string) predicate.Service {
	switch strings.ToLower(currency) {
	case "xmr":
		return service.Xmr(true)
	case "btc":
		return service.Btc(true)
	case "ln":
		return service.Lightning(true)
	case "cash":
		return service.Cash(true)
	case "fiat":
		return service.Fiat(true)
	default:
		return nil
	}
}
