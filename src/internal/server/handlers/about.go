package handlers

import (
	"bytes"
	"sync"

	"github.com/kataras/iris/v12"

	"kycnot.me/frontend/templates"
)

var (
	aboutOnce sync.Once
	aboutHTML string
)

func About(c iris.Context) {
	aboutOnce.Do(func() {
		buf := &bytes.Buffer{}
		err := templates.About().Render(c.Request().Context(), buf)
		if err != nil {
			// Handle error appropriately
			return
		}
		aboutHTML = buf.String()
	})

	c.HTML(aboutHTML)
}
