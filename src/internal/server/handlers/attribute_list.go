package handlers

import (
	"context"

	"github.com/kataras/iris/v12"

	"kycnot.me/frontend/templates"
	"kycnot.me/internal/database"
	"kycnot.me/internal/ent"
	"kycnot.me/internal/ent/attribute"
)

func AttributeList(c iris.Context) {
	aa, _ := database.Client().Attribute.Query().
		Order(ent.Asc(attribute.FieldRating)).
		All(context.Background())

	// Cache service rendered component for 30 minutes
	component := templates.AttributeList(aa)
	// cache.Cache.SetWithTTL(cacheKey, component, 1, 30*time.Minute)
	c.RenderComponent(component)
}
