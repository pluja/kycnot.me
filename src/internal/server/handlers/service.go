package handlers

import (
	"context"
	"fmt"
	"sort"

	slugger "github.com/gosimple/slug"
	"github.com/kataras/iris/v12"
	"github.com/phuslu/log"

	"kycnot.me/frontend/templates"
	"kycnot.me/internal/database"
	"kycnot.me/internal/ent"
	"kycnot.me/internal/ent/attribute"
	"kycnot.me/internal/ent/service"
	"kycnot.me/utils"
)

func Service(c iris.Context) {
	slug := c.Params().Get("slug")
	if !slugger.IsSlug(slug) {
		c.Redirect(fmt.Sprintf("/service/%s", slugger.Make(slug)))
	}

	// TODO: Explore if enabling cache is worht it
	// cacheKey := "service-rendered-" + slug
	// cachedHTML, found := cache.Cache.Get(cacheKey)
	// if found {
	// 	if htmlStr, ok := cachedHTML.(string); ok {
	// 		c.HTML(htmlStr)
	// 		return
	// 	}
	// }

	s, err := database.Client().Service.Query().
		Where(service.SlugEQ(slug)).
		Order(ent.Asc(service.FieldID)).
		WithAttributes(func(q *ent.AttributeQuery) {
			q.Order(ent.Desc(attribute.FieldRating))
		}).
		First(context.Background())
	if err != nil {
		log.Debug().Err(err).Msgf("error")
		c.Redirect("/?error=Service not found", iris.StatusSeeOther)
		return
	}

	// Move sorting after error check
	if s.Edges.Attributes != nil {
		sort.Slice(s.Edges.Attributes, func(i, j int) bool {
			ratingOrder := map[attribute.Rating]int{
				attribute.RatingBad:  1,
				attribute.RatingWarn: 2,
				attribute.RatingGood: 3,
				attribute.RatingInfo: 4,
			}
			return ratingOrder[s.Edges.Attributes[i].Rating] < ratingOrder[s.Edges.Attributes[j].Rating]
		})
	}

	if err != nil {
		log.Debug().Err(err).Msgf("error")
		c.Redirect("/?error=Service not found", iris.StatusSeeOther)
		return
	}

	if s.LogoURL != "" {
		go utils.GetImage(s.LogoURL, s.Slug)
	}

	/// CACHE LOGIC
	// Render the component to HTML string
	// component := templates.Service(s)
	// buf := &bytes.Buffer{}
	// err = component.Render(c.Request().Context(), buf)
	// if err != nil {
	// 	log.Debug().Err(err).Msgf("error rendering template")
	// 	c.Redirect("/?error=Internal error", iris.StatusSeeOther)
	// 	return
	// }

	// htmlStr := buf.String()
	// Cache the rendered HTML for 30 minutes
	// cache.Cache.SetWithTTL(cacheKey, htmlStr, 1, 30*time.Minute)
	// c.HTML(htmlStr)
	/// END CACHE LOGIC

	c.RenderComponent(templates.Service(s))
}
