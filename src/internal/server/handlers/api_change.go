package handlers

import (
	"context"
	"net/http"
	"time"

	"github.com/kataras/iris/v12"
	"github.com/phuslu/log"

	"kycnot.me/internal/database"
)

type ChangeCreateRequest struct {
	Title     string    `json:"title"`
	Summary   string    `json:"summary"`
	Service   string    `json:"service-slug"`
	Warning   bool      `json:"warning"`
	CreatedAt time.Time `json:"created-at"`
}

func ApiChangePost(c iris.Context) {
	var req ChangeCreateRequest
	if err := c.ReadJSON(&req); err != nil {
		c.StatusCode(http.StatusBadRequest)
		log.Error().AnErr("error", err).Str("change", req.Title)
		c.JSON(iris.Map{"error": "Invalid request body: " + err.Error()})
		return
	}

	svc, err := database.Client().Change.Create().
		SetTitle(req.Title).
		SetSummary(req.Summary).
		SetWarning(req.Warning).
		SetCreatedAt(req.CreatedAt).
		Save(context.Background())
	if err != nil {
		c.StatusCode(http.StatusInternalServerError)
		log.Error().AnErr("error", err).Str("change", req.Title)
		c.JSON(iris.Map{"error": err.Error()})
		return
	}

	c.StatusCode(http.StatusCreated)
	c.JSON(svc)
}
