package handlers

import (
	"context"
	"fmt"

	slugger "github.com/gosimple/slug"
	"github.com/kataras/iris/v12"
	"github.com/phuslu/log"
	"kycnot.me/internal/database"
	"kycnot.me/internal/ent"
	"kycnot.me/internal/ent/service"
	"kycnot.me/utils"
)

func ServiceSummary(c iris.Context) {
	slug := c.Params().Get("slug")
	if !slugger.IsSlug(slug) {
		c.Redirect(fmt.Sprintf("/service/%s/summary", slugger.Make(slug)))
	}

	s, err := database.Client().Service.Query().
		Where(service.SlugEQ(slug)).
		Order(ent.Asc(service.FieldID)).
		WithAttributes().
		First(context.Background())
	if err != nil {
		log.Debug().Err(err).Msgf("error")
		c.Redirect("/?error=Service not found", iris.StatusSeeOther)
		return
	}

	score := utils.ComputeScore(s)

	c.Text(score.Summary)
}
