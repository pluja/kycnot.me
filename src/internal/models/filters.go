package models

type ServiceFilters struct {
	Currencies map[string]bool `json:"currencies"`
	Query      string          `json:"query"`
	Type       string          `json:"type"`
}
