// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"kycnot.me/internal/ent/change"
	"kycnot.me/internal/ent/service"
)

// ChangeCreate is the builder for creating a Change entity.
type ChangeCreate struct {
	config
	mutation *ChangeMutation
	hooks    []Hook
}

// SetTitle sets the "Title" field.
func (cc *ChangeCreate) SetTitle(s string) *ChangeCreate {
	cc.mutation.SetTitle(s)
	return cc
}

// SetNillableTitle sets the "Title" field if the given value is not nil.
func (cc *ChangeCreate) SetNillableTitle(s *string) *ChangeCreate {
	if s != nil {
		cc.SetTitle(*s)
	}
	return cc
}

// SetSummary sets the "Summary" field.
func (cc *ChangeCreate) SetSummary(s string) *ChangeCreate {
	cc.mutation.SetSummary(s)
	return cc
}

// SetNillableSummary sets the "Summary" field if the given value is not nil.
func (cc *ChangeCreate) SetNillableSummary(s *string) *ChangeCreate {
	if s != nil {
		cc.SetSummary(*s)
	}
	return cc
}

// SetCreatedAt sets the "createdAt" field.
func (cc *ChangeCreate) SetCreatedAt(t time.Time) *ChangeCreate {
	cc.mutation.SetCreatedAt(t)
	return cc
}

// SetNillableCreatedAt sets the "createdAt" field if the given value is not nil.
func (cc *ChangeCreate) SetNillableCreatedAt(t *time.Time) *ChangeCreate {
	if t != nil {
		cc.SetCreatedAt(*t)
	}
	return cc
}

// SetWarning sets the "warning" field.
func (cc *ChangeCreate) SetWarning(b bool) *ChangeCreate {
	cc.mutation.SetWarning(b)
	return cc
}

// SetNillableWarning sets the "warning" field if the given value is not nil.
func (cc *ChangeCreate) SetNillableWarning(b *bool) *ChangeCreate {
	if b != nil {
		cc.SetWarning(*b)
	}
	return cc
}

// SetServiceID sets the "service" edge to the Service entity by ID.
func (cc *ChangeCreate) SetServiceID(id string) *ChangeCreate {
	cc.mutation.SetServiceID(id)
	return cc
}

// SetNillableServiceID sets the "service" edge to the Service entity by ID if the given value is not nil.
func (cc *ChangeCreate) SetNillableServiceID(id *string) *ChangeCreate {
	if id != nil {
		cc = cc.SetServiceID(*id)
	}
	return cc
}

// SetService sets the "service" edge to the Service entity.
func (cc *ChangeCreate) SetService(s *Service) *ChangeCreate {
	return cc.SetServiceID(s.ID)
}

// Mutation returns the ChangeMutation object of the builder.
func (cc *ChangeCreate) Mutation() *ChangeMutation {
	return cc.mutation
}

// Save creates the Change in the database.
func (cc *ChangeCreate) Save(ctx context.Context) (*Change, error) {
	cc.defaults()
	return withHooks(ctx, cc.sqlSave, cc.mutation, cc.hooks)
}

// SaveX calls Save and panics if Save returns an error.
func (cc *ChangeCreate) SaveX(ctx context.Context) *Change {
	v, err := cc.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (cc *ChangeCreate) Exec(ctx context.Context) error {
	_, err := cc.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (cc *ChangeCreate) ExecX(ctx context.Context) {
	if err := cc.Exec(ctx); err != nil {
		panic(err)
	}
}

// defaults sets the default values of the builder before save.
func (cc *ChangeCreate) defaults() {
	if _, ok := cc.mutation.CreatedAt(); !ok {
		v := change.DefaultCreatedAt()
		cc.mutation.SetCreatedAt(v)
	}
	if _, ok := cc.mutation.Warning(); !ok {
		v := change.DefaultWarning
		cc.mutation.SetWarning(v)
	}
}

// check runs all checks and user-defined validators on the builder.
func (cc *ChangeCreate) check() error {
	if _, ok := cc.mutation.CreatedAt(); !ok {
		return &ValidationError{Name: "createdAt", err: errors.New(`ent: missing required field "Change.createdAt"`)}
	}
	if _, ok := cc.mutation.Warning(); !ok {
		return &ValidationError{Name: "warning", err: errors.New(`ent: missing required field "Change.warning"`)}
	}
	return nil
}

func (cc *ChangeCreate) sqlSave(ctx context.Context) (*Change, error) {
	if err := cc.check(); err != nil {
		return nil, err
	}
	_node, _spec := cc.createSpec()
	if err := sqlgraph.CreateNode(ctx, cc.driver, _spec); err != nil {
		if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	id := _spec.ID.Value.(int64)
	_node.ID = int(id)
	cc.mutation.id = &_node.ID
	cc.mutation.done = true
	return _node, nil
}

func (cc *ChangeCreate) createSpec() (*Change, *sqlgraph.CreateSpec) {
	var (
		_node = &Change{config: cc.config}
		_spec = sqlgraph.NewCreateSpec(change.Table, sqlgraph.NewFieldSpec(change.FieldID, field.TypeInt))
	)
	if value, ok := cc.mutation.Title(); ok {
		_spec.SetField(change.FieldTitle, field.TypeString, value)
		_node.Title = value
	}
	if value, ok := cc.mutation.Summary(); ok {
		_spec.SetField(change.FieldSummary, field.TypeString, value)
		_node.Summary = value
	}
	if value, ok := cc.mutation.CreatedAt(); ok {
		_spec.SetField(change.FieldCreatedAt, field.TypeTime, value)
		_node.CreatedAt = value
	}
	if value, ok := cc.mutation.Warning(); ok {
		_spec.SetField(change.FieldWarning, field.TypeBool, value)
		_node.Warning = value
	}
	if nodes := cc.mutation.ServiceIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2O,
			Inverse: true,
			Table:   change.ServiceTable,
			Columns: []string{change.ServiceColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: sqlgraph.NewFieldSpec(service.FieldID, field.TypeString),
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_node.service_changes = &nodes[0]
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// ChangeCreateBulk is the builder for creating many Change entities in bulk.
type ChangeCreateBulk struct {
	config
	err      error
	builders []*ChangeCreate
}

// Save creates the Change entities in the database.
func (ccb *ChangeCreateBulk) Save(ctx context.Context) ([]*Change, error) {
	if ccb.err != nil {
		return nil, ccb.err
	}
	specs := make([]*sqlgraph.CreateSpec, len(ccb.builders))
	nodes := make([]*Change, len(ccb.builders))
	mutators := make([]Mutator, len(ccb.builders))
	for i := range ccb.builders {
		func(i int, root context.Context) {
			builder := ccb.builders[i]
			builder.defaults()
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*ChangeMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				var err error
				nodes[i], specs[i] = builder.createSpec()
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, ccb.builders[i+1].mutation)
				} else {
					spec := &sqlgraph.BatchCreateSpec{Nodes: specs}
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, ccb.driver, spec); err != nil {
						if sqlgraph.IsConstraintError(err) {
							err = &ConstraintError{msg: err.Error(), wrap: err}
						}
					}
				}
				if err != nil {
					return nil, err
				}
				mutation.id = &nodes[i].ID
				if specs[i].ID.Value != nil {
					id := specs[i].ID.Value.(int64)
					nodes[i].ID = int(id)
				}
				mutation.done = true
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, ccb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (ccb *ChangeCreateBulk) SaveX(ctx context.Context) []*Change {
	v, err := ccb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (ccb *ChangeCreateBulk) Exec(ctx context.Context) error {
	_, err := ccb.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (ccb *ChangeCreateBulk) ExecX(ctx context.Context) {
	if err := ccb.Exec(ctx); err != nil {
		panic(err)
	}
}
