// Code generated by ent, DO NOT EDIT.

package ent

import (
	"fmt"
	"strings"

	"entgo.io/ent"
	"entgo.io/ent/dialect/sql"
	"kycnot.me/internal/ent/announcement"
)

// Announcement is the model entity for the Announcement schema.
type Announcement struct {
	config `json:"-"`
	// ID of the ent.
	ID int `json:"id,omitempty"`
	// The main content of the announcement
	Body string `json:"body,omitempty"`
	// The title of the announcement
	Title string `json:"title,omitempty"`
	// URL link associated with the announcement
	Link string `json:"link,omitempty"`
	// Indicates if this is a warning announcement
	Warning bool `json:"warning,omitempty"`
	// Indicates if the announcement is currently active
	Active       bool `json:"active,omitempty"`
	selectValues sql.SelectValues
}

// scanValues returns the types for scanning values from sql.Rows.
func (*Announcement) scanValues(columns []string) ([]any, error) {
	values := make([]any, len(columns))
	for i := range columns {
		switch columns[i] {
		case announcement.FieldWarning, announcement.FieldActive:
			values[i] = new(sql.NullBool)
		case announcement.FieldID:
			values[i] = new(sql.NullInt64)
		case announcement.FieldBody, announcement.FieldTitle, announcement.FieldLink:
			values[i] = new(sql.NullString)
		default:
			values[i] = new(sql.UnknownType)
		}
	}
	return values, nil
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the Announcement fields.
func (a *Announcement) assignValues(columns []string, values []any) error {
	if m, n := len(values), len(columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	for i := range columns {
		switch columns[i] {
		case announcement.FieldID:
			value, ok := values[i].(*sql.NullInt64)
			if !ok {
				return fmt.Errorf("unexpected type %T for field id", value)
			}
			a.ID = int(value.Int64)
		case announcement.FieldBody:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field body", values[i])
			} else if value.Valid {
				a.Body = value.String
			}
		case announcement.FieldTitle:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field title", values[i])
			} else if value.Valid {
				a.Title = value.String
			}
		case announcement.FieldLink:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field link", values[i])
			} else if value.Valid {
				a.Link = value.String
			}
		case announcement.FieldWarning:
			if value, ok := values[i].(*sql.NullBool); !ok {
				return fmt.Errorf("unexpected type %T for field warning", values[i])
			} else if value.Valid {
				a.Warning = value.Bool
			}
		case announcement.FieldActive:
			if value, ok := values[i].(*sql.NullBool); !ok {
				return fmt.Errorf("unexpected type %T for field active", values[i])
			} else if value.Valid {
				a.Active = value.Bool
			}
		default:
			a.selectValues.Set(columns[i], values[i])
		}
	}
	return nil
}

// Value returns the ent.Value that was dynamically selected and assigned to the Announcement.
// This includes values selected through modifiers, order, etc.
func (a *Announcement) Value(name string) (ent.Value, error) {
	return a.selectValues.Get(name)
}

// Update returns a builder for updating this Announcement.
// Note that you need to call Announcement.Unwrap() before calling this method if this Announcement
// was returned from a transaction, and the transaction was committed or rolled back.
func (a *Announcement) Update() *AnnouncementUpdateOne {
	return NewAnnouncementClient(a.config).UpdateOne(a)
}

// Unwrap unwraps the Announcement entity that was returned from a transaction after it was closed,
// so that all future queries will be executed through the driver which created the transaction.
func (a *Announcement) Unwrap() *Announcement {
	_tx, ok := a.config.driver.(*txDriver)
	if !ok {
		panic("ent: Announcement is not a transactional entity")
	}
	a.config.driver = _tx.drv
	return a
}

// String implements the fmt.Stringer.
func (a *Announcement) String() string {
	var builder strings.Builder
	builder.WriteString("Announcement(")
	builder.WriteString(fmt.Sprintf("id=%v, ", a.ID))
	builder.WriteString("body=")
	builder.WriteString(a.Body)
	builder.WriteString(", ")
	builder.WriteString("title=")
	builder.WriteString(a.Title)
	builder.WriteString(", ")
	builder.WriteString("link=")
	builder.WriteString(a.Link)
	builder.WriteString(", ")
	builder.WriteString("warning=")
	builder.WriteString(fmt.Sprintf("%v", a.Warning))
	builder.WriteString(", ")
	builder.WriteString("active=")
	builder.WriteString(fmt.Sprintf("%v", a.Active))
	builder.WriteByte(')')
	return builder.String()
}

// Announcements is a parsable slice of Announcement.
type Announcements []*Announcement
