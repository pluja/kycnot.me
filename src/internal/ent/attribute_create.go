// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"kycnot.me/internal/ent/attribute"
	"kycnot.me/internal/ent/service"
)

// AttributeCreate is the builder for creating a Attribute entity.
type AttributeCreate struct {
	config
	mutation *AttributeMutation
	hooks    []Hook
}

// SetCreatedAt sets the "createdAt" field.
func (ac *AttributeCreate) SetCreatedAt(t time.Time) *AttributeCreate {
	ac.mutation.SetCreatedAt(t)
	return ac
}

// SetNillableCreatedAt sets the "createdAt" field if the given value is not nil.
func (ac *AttributeCreate) SetNillableCreatedAt(t *time.Time) *AttributeCreate {
	if t != nil {
		ac.SetCreatedAt(*t)
	}
	return ac
}

// SetTitle sets the "title" field.
func (ac *AttributeCreate) SetTitle(s string) *AttributeCreate {
	ac.mutation.SetTitle(s)
	return ac
}

// SetBonus sets the "bonus" field.
func (ac *AttributeCreate) SetBonus(f float64) *AttributeCreate {
	ac.mutation.SetBonus(f)
	return ac
}

// SetNillableBonus sets the "bonus" field if the given value is not nil.
func (ac *AttributeCreate) SetNillableBonus(f *float64) *AttributeCreate {
	if f != nil {
		ac.SetBonus(*f)
	}
	return ac
}

// SetSlug sets the "slug" field.
func (ac *AttributeCreate) SetSlug(s string) *AttributeCreate {
	ac.mutation.SetSlug(s)
	return ac
}

// SetRating sets the "rating" field.
func (ac *AttributeCreate) SetRating(a attribute.Rating) *AttributeCreate {
	ac.mutation.SetRating(a)
	return ac
}

// SetDescription sets the "description" field.
func (ac *AttributeCreate) SetDescription(s string) *AttributeCreate {
	ac.mutation.SetDescription(s)
	return ac
}

// SetNillableDescription sets the "description" field if the given value is not nil.
func (ac *AttributeCreate) SetNillableDescription(s *string) *AttributeCreate {
	if s != nil {
		ac.SetDescription(*s)
	}
	return ac
}

// SetID sets the "id" field.
func (ac *AttributeCreate) SetID(s string) *AttributeCreate {
	ac.mutation.SetID(s)
	return ac
}

// SetNillableID sets the "id" field if the given value is not nil.
func (ac *AttributeCreate) SetNillableID(s *string) *AttributeCreate {
	if s != nil {
		ac.SetID(*s)
	}
	return ac
}

// AddServiceIDs adds the "services" edge to the Service entity by IDs.
func (ac *AttributeCreate) AddServiceIDs(ids ...string) *AttributeCreate {
	ac.mutation.AddServiceIDs(ids...)
	return ac
}

// AddServices adds the "services" edges to the Service entity.
func (ac *AttributeCreate) AddServices(s ...*Service) *AttributeCreate {
	ids := make([]string, len(s))
	for i := range s {
		ids[i] = s[i].ID
	}
	return ac.AddServiceIDs(ids...)
}

// Mutation returns the AttributeMutation object of the builder.
func (ac *AttributeCreate) Mutation() *AttributeMutation {
	return ac.mutation
}

// Save creates the Attribute in the database.
func (ac *AttributeCreate) Save(ctx context.Context) (*Attribute, error) {
	ac.defaults()
	return withHooks(ctx, ac.sqlSave, ac.mutation, ac.hooks)
}

// SaveX calls Save and panics if Save returns an error.
func (ac *AttributeCreate) SaveX(ctx context.Context) *Attribute {
	v, err := ac.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (ac *AttributeCreate) Exec(ctx context.Context) error {
	_, err := ac.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (ac *AttributeCreate) ExecX(ctx context.Context) {
	if err := ac.Exec(ctx); err != nil {
		panic(err)
	}
}

// defaults sets the default values of the builder before save.
func (ac *AttributeCreate) defaults() {
	if _, ok := ac.mutation.CreatedAt(); !ok {
		v := attribute.DefaultCreatedAt()
		ac.mutation.SetCreatedAt(v)
	}
	if _, ok := ac.mutation.Bonus(); !ok {
		v := attribute.DefaultBonus
		ac.mutation.SetBonus(v)
	}
	if _, ok := ac.mutation.ID(); !ok {
		v := attribute.DefaultID()
		ac.mutation.SetID(v)
	}
}

// check runs all checks and user-defined validators on the builder.
func (ac *AttributeCreate) check() error {
	if _, ok := ac.mutation.CreatedAt(); !ok {
		return &ValidationError{Name: "createdAt", err: errors.New(`ent: missing required field "Attribute.createdAt"`)}
	}
	if _, ok := ac.mutation.Title(); !ok {
		return &ValidationError{Name: "title", err: errors.New(`ent: missing required field "Attribute.title"`)}
	}
	if _, ok := ac.mutation.Bonus(); !ok {
		return &ValidationError{Name: "bonus", err: errors.New(`ent: missing required field "Attribute.bonus"`)}
	}
	if _, ok := ac.mutation.Slug(); !ok {
		return &ValidationError{Name: "slug", err: errors.New(`ent: missing required field "Attribute.slug"`)}
	}
	if _, ok := ac.mutation.Rating(); !ok {
		return &ValidationError{Name: "rating", err: errors.New(`ent: missing required field "Attribute.rating"`)}
	}
	if v, ok := ac.mutation.Rating(); ok {
		if err := attribute.RatingValidator(v); err != nil {
			return &ValidationError{Name: "rating", err: fmt.Errorf(`ent: validator failed for field "Attribute.rating": %w`, err)}
		}
	}
	return nil
}

func (ac *AttributeCreate) sqlSave(ctx context.Context) (*Attribute, error) {
	if err := ac.check(); err != nil {
		return nil, err
	}
	_node, _spec := ac.createSpec()
	if err := sqlgraph.CreateNode(ctx, ac.driver, _spec); err != nil {
		if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	if _spec.ID.Value != nil {
		if id, ok := _spec.ID.Value.(string); ok {
			_node.ID = id
		} else {
			return nil, fmt.Errorf("unexpected Attribute.ID type: %T", _spec.ID.Value)
		}
	}
	ac.mutation.id = &_node.ID
	ac.mutation.done = true
	return _node, nil
}

func (ac *AttributeCreate) createSpec() (*Attribute, *sqlgraph.CreateSpec) {
	var (
		_node = &Attribute{config: ac.config}
		_spec = sqlgraph.NewCreateSpec(attribute.Table, sqlgraph.NewFieldSpec(attribute.FieldID, field.TypeString))
	)
	if id, ok := ac.mutation.ID(); ok {
		_node.ID = id
		_spec.ID.Value = id
	}
	if value, ok := ac.mutation.CreatedAt(); ok {
		_spec.SetField(attribute.FieldCreatedAt, field.TypeTime, value)
		_node.CreatedAt = value
	}
	if value, ok := ac.mutation.Title(); ok {
		_spec.SetField(attribute.FieldTitle, field.TypeString, value)
		_node.Title = value
	}
	if value, ok := ac.mutation.Bonus(); ok {
		_spec.SetField(attribute.FieldBonus, field.TypeFloat64, value)
		_node.Bonus = value
	}
	if value, ok := ac.mutation.Slug(); ok {
		_spec.SetField(attribute.FieldSlug, field.TypeString, value)
		_node.Slug = value
	}
	if value, ok := ac.mutation.Rating(); ok {
		_spec.SetField(attribute.FieldRating, field.TypeEnum, value)
		_node.Rating = value
	}
	if value, ok := ac.mutation.Description(); ok {
		_spec.SetField(attribute.FieldDescription, field.TypeString, value)
		_node.Description = value
	}
	if nodes := ac.mutation.ServicesIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.M2M,
			Inverse: true,
			Table:   attribute.ServicesTable,
			Columns: attribute.ServicesPrimaryKey,
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: sqlgraph.NewFieldSpec(service.FieldID, field.TypeString),
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// AttributeCreateBulk is the builder for creating many Attribute entities in bulk.
type AttributeCreateBulk struct {
	config
	err      error
	builders []*AttributeCreate
}

// Save creates the Attribute entities in the database.
func (acb *AttributeCreateBulk) Save(ctx context.Context) ([]*Attribute, error) {
	if acb.err != nil {
		return nil, acb.err
	}
	specs := make([]*sqlgraph.CreateSpec, len(acb.builders))
	nodes := make([]*Attribute, len(acb.builders))
	mutators := make([]Mutator, len(acb.builders))
	for i := range acb.builders {
		func(i int, root context.Context) {
			builder := acb.builders[i]
			builder.defaults()
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*AttributeMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				var err error
				nodes[i], specs[i] = builder.createSpec()
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, acb.builders[i+1].mutation)
				} else {
					spec := &sqlgraph.BatchCreateSpec{Nodes: specs}
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, acb.driver, spec); err != nil {
						if sqlgraph.IsConstraintError(err) {
							err = &ConstraintError{msg: err.Error(), wrap: err}
						}
					}
				}
				if err != nil {
					return nil, err
				}
				mutation.id = &nodes[i].ID
				mutation.done = true
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, acb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (acb *AttributeCreateBulk) SaveX(ctx context.Context) []*Attribute {
	v, err := acb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (acb *AttributeCreateBulk) Exec(ctx context.Context) error {
	_, err := acb.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (acb *AttributeCreateBulk) ExecX(ctx context.Context) {
	if err := acb.Exec(ctx); err != nil {
		panic(err)
	}
}
