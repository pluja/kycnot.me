package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"

	"kycnot.me/utils/ulid"
)

// Service holds the schema definition for the Service entity.
type Service struct {
	ent.Schema
}

type TosReview struct {
	Title     string `json:"title"`
	Details   string `json:"details"`
	Section   string `json:"section"`
	Warning   bool   `json:"warning"`
	Reference string `json:"reference"`
}

type SupportMethod struct {
	Icon string `json:"icon"`
	Text string `json:"text"`
	Link string `json:"link"`
}

// Fields of the Service.
func (Service) Fields() []ent.Field {
	return []ent.Field{
		field.String("id").DefaultFunc(ulid.UniqueULID).Immutable().StructTag(`json:"id"`),
		field.Time("createdAt").
			Default(time.Now).
			StructTag(`json:"createdAt"`).
			Annotations(
				entsql.Default("CURRENT_TIMESTAMP"),
			),
		field.String("name").
			Comment("The name of the service").
			StructTag(`json:"name,omitempty"`),
		field.String("slug").
			Unique().
			NotEmpty().
			Comment("The slug of the service").
			StructTag(`json:"slug,omitempty"`),
		field.String("description").
			NotEmpty().
			MaxLen(532).
			Comment("Description of the service").
			StructTag(`json:"description"`),
		field.String("logo_url").
			Optional().
			Comment("URL of the service logo").
			StructTag(`json:"logo_url,omitempty"`),
		field.JSON("tos_urls", []string{}).
			Optional().
			Comment("Terms of Service URLs").
			StructTag(`json:"tos_urls,omitempty"`),
		field.JSON("onion_urls", []string{}).
			Optional().
			Comment("Onion (Tor) URLs for the service").
			StructTag(`json:"onion_urls,omitempty"`),
		field.String("category").
			Optional().
			Comment("Category of the service").
			StructTag(`json:"category,omitempty"`),
		field.JSON("comments", []string{}).
			Optional().
			Comment("User comments about the service").
			StructTag(`json:"comments,omitempty"`),
		field.JSON("tos_reviews", []TosReview{}).
			Optional().
			Comment("Reviews of the Terms of Service").
			StructTag(`json:"tos_reviews,omitempty"`),
		field.JSON("urls", []string{}).
			Comment("URLs associated with the service").
			StructTag(`json:"urls"`),
		field.Strings("tags").
			Optional().
			Comment("Tags associated with the service").
			StructTag(`json:"tags,omitempty"`),
		field.Int("kyc_level").
			Optional().
			Range(0, 3).
			Comment("Know Your Customer level (0-3)").
			StructTag(`json:"kyc_level,omitempty"`),
		field.Enum("type").
			Values("exchange", "service").
			Default("exchange").
			Comment("Type of the service").
			StructTag(`json:"type,omitempty"`),
		field.Bool("listed").
			Default(false).
			Comment("Whether the service is listed").
			StructTag(`json:"listed,omitempty"`),
		field.Bool("verified").
			Default(false).
			Comment("Whether the service is verified").
			StructTag(`json:"verified,omitempty"`),
		field.Bool("pending").
			Default(true).
			Comment("Whether the service is pending approval").
			StructTag(`json:"pending,omitempty"`),
		field.Bool("xmr").
			Default(false).
			Comment("Supports Monero (XMR)").
			StructTag(`json:"xmr,omitempty"`),
		field.Bool("btc").
			Default(false).
			Comment("Supports Bitcoin (BTC)").
			StructTag(`json:"btc,omitempty"`),
		field.Bool("lightning").
			Default(false).
			Comment("Supports Lightning Network").
			StructTag(`json:"lightning,omitempty"`),
		field.Bool("cash").
			Default(false).
			Comment("Supports cash transactions").
			StructTag(`json:"cash,omitempty"`),
		field.Bool("fiat").
			Default(false).
			Comment("Supports fiat currency").
			StructTag(`json:"fiat,omitempty"`),
		field.Time("last_tos_review").
			Optional().
			Comment("Date of the last Terms of Service review").
			StructTag(`json:"last_tos_review,omitempty"`),
		field.String("referral").
			Optional().
			Comment("Referral information").
			StructTag(`json:"referral,omitempty"`),
		field.Int("score").
			Optional().
			Range(0, 10).
			Comment("Service score (0-10)").
			StructTag(`json:"score,omitempty"`),
		field.Bool("potential_scam").
			Default(false).
			Comment("Flag for potential scam").
			StructTag(`json:"potential_scam,omitempty"`),
		field.Strings("notes").
			Optional().
			Comment("Additional notes about the service").
			StructTag(`json:"notes,omitempty"`),
		field.JSON("support", []SupportMethod{}).
			Optional().
			Comment("Support information").
			StructTag(`json:"support,omitempty"`),
	}
}

// Edges of the Service.
func (Service) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("attributes", Attribute.Type).
			Comment("Attributes associated with the service").
			StructTag(`json:"attributes,omitempty"`),

		edge.To("changes", Change.Type).
			Comment("Changes associated with the service").
			StructTag(`json:"changes,omitempty"`),
	}
}

// Indexes of the Service.
func (Service) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("name", "category"),
		index.Fields("slug", "category"),
		index.Fields("slug"),
		index.Fields("type", "listed", "verified"),
		index.Fields("xmr", "btc", "lightning", "cash", "fiat"),
		index.Fields("kyc_level", "score"),
	}
}
