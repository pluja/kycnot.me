package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"

	"kycnot.me/utils/ulid"
)

// Attribute holds the schema definition for the Attribute entity.
type Attribute struct {
	ent.Schema
}

// Fields of the Attribute.
func (Attribute) Fields() []ent.Field {
	return []ent.Field{
		field.String("id").DefaultFunc(ulid.UniqueULID).Immutable().StructTag(`json:"id"`),
		field.Time("createdAt").
			Default(time.Now).
			StructTag(`json:"createdAt"`).
			Annotations(
				entsql.Default("CURRENT_TIMESTAMP"),
			),
		field.String("title").
			Comment("Title of the attribute").
			StructTag(`json:"title,omitempty"`),
		field.Float("bonus").
			Default(0).
			Comment("Bonus of the attribute").
			StructTag(`json:"bonus,omitempty"`),
		field.String("slug").
			Unique().
			Comment("The slug of the service").
			StructTag(`json:"slug,omitempty"`),
		field.Enum("rating").
			Values("good", "bad", "warn", "info").
			Comment("Rating of the attribute").
			StructTag(`json:"rating,omitempty"`),
		field.Text("description").
			Optional().
			Comment("Description of the attribute").
			StructTag(`json:"description,omitempty"`),
	}
}

// Edges of the Attribute.
func (Attribute) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("services", Service.Type).
			Ref("attributes").
			Comment("Services associated with this attribute").
			StructTag(`json:"services,omitempty"`),
	}
}

// Indexes of the Attribute.
func (Attribute) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("title", "rating"),
		index.Fields("slug"),
	}
}
