package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

type Change struct {
	ent.Schema
}

func (Change) Fields() []ent.Field {
	return []ent.Field{
		field.String("Title").
			Optional().
			Comment("The title of the change").
			StructTag(`json:"Title,omitempty"`),
		field.Text("Summary").
			Optional().
			Comment("A summary of the change").
			StructTag(`json:"Summary,omitempty"`),
		field.Time("createdAt").
			Default(time.Now).
			StructTag(`json:"createdAt"`).
			Immutable().
			Annotations(
				entsql.Default("CURRENT_TIMESTAMP"),
			),

		field.Bool("warning").
			Default(false).
			StructTag(`json:"warning"`),
	}
}

func (Change) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("service", Service.Type).
			Ref("changes").
			Unique().
			Comment("The service associated with this change").
			StructTag(`json:"service,omitempty"`),
	}
}

func (Change) Indexes() []ent.Index {
	return nil
}
