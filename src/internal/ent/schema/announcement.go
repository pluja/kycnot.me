package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
)

// Announcement holds the schema definition for the Announcement entity.
type Announcement struct {
	ent.Schema
}

// Fields of the Announcement.
func (Announcement) Fields() []ent.Field {
	return []ent.Field{
		field.Text("body").
			Optional().
			Comment("The main content of the announcement").
			StructTag(`json:"body,omitempty"`),

		field.String("title").
			Optional().
			Comment("The title of the announcement").
			StructTag(`json:"title,omitempty"`),

		field.String("link").
			Optional().
			Comment("URL link associated with the announcement").
			StructTag(`json:"link,omitempty"`),

		field.Bool("warning").
			Optional().
			Comment("Indicates if this is a warning announcement").
			StructTag(`json:"warning,omitempty"`),

		field.Bool("active").
			Optional().
			Comment("Indicates if the announcement is currently active").
			StructTag(`json:"active,omitempty"`),
	}
}

// Edges of the Announcement.
func (Announcement) Edges() []ent.Edge {
	return nil
}

// Indexes of the Announcement.
func (Announcement) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("active"),
	}
}
