// Code generated by ent, DO NOT EDIT.

package change

import (
	"time"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"kycnot.me/internal/ent/predicate"
)

// ID filters vertices based on their ID field.
func ID(id int) predicate.Change {
	return predicate.Change(sql.FieldEQ(FieldID, id))
}

// IDEQ applies the EQ predicate on the ID field.
func IDEQ(id int) predicate.Change {
	return predicate.Change(sql.FieldEQ(FieldID, id))
}

// IDNEQ applies the NEQ predicate on the ID field.
func IDNEQ(id int) predicate.Change {
	return predicate.Change(sql.FieldNEQ(FieldID, id))
}

// IDIn applies the In predicate on the ID field.
func IDIn(ids ...int) predicate.Change {
	return predicate.Change(sql.FieldIn(FieldID, ids...))
}

// IDNotIn applies the NotIn predicate on the ID field.
func IDNotIn(ids ...int) predicate.Change {
	return predicate.Change(sql.FieldNotIn(FieldID, ids...))
}

// IDGT applies the GT predicate on the ID field.
func IDGT(id int) predicate.Change {
	return predicate.Change(sql.FieldGT(FieldID, id))
}

// IDGTE applies the GTE predicate on the ID field.
func IDGTE(id int) predicate.Change {
	return predicate.Change(sql.FieldGTE(FieldID, id))
}

// IDLT applies the LT predicate on the ID field.
func IDLT(id int) predicate.Change {
	return predicate.Change(sql.FieldLT(FieldID, id))
}

// IDLTE applies the LTE predicate on the ID field.
func IDLTE(id int) predicate.Change {
	return predicate.Change(sql.FieldLTE(FieldID, id))
}

// Title applies equality check predicate on the "Title" field. It's identical to TitleEQ.
func Title(v string) predicate.Change {
	return predicate.Change(sql.FieldEQ(FieldTitle, v))
}

// Summary applies equality check predicate on the "Summary" field. It's identical to SummaryEQ.
func Summary(v string) predicate.Change {
	return predicate.Change(sql.FieldEQ(FieldSummary, v))
}

// CreatedAt applies equality check predicate on the "createdAt" field. It's identical to CreatedAtEQ.
func CreatedAt(v time.Time) predicate.Change {
	return predicate.Change(sql.FieldEQ(FieldCreatedAt, v))
}

// Warning applies equality check predicate on the "warning" field. It's identical to WarningEQ.
func Warning(v bool) predicate.Change {
	return predicate.Change(sql.FieldEQ(FieldWarning, v))
}

// TitleEQ applies the EQ predicate on the "Title" field.
func TitleEQ(v string) predicate.Change {
	return predicate.Change(sql.FieldEQ(FieldTitle, v))
}

// TitleNEQ applies the NEQ predicate on the "Title" field.
func TitleNEQ(v string) predicate.Change {
	return predicate.Change(sql.FieldNEQ(FieldTitle, v))
}

// TitleIn applies the In predicate on the "Title" field.
func TitleIn(vs ...string) predicate.Change {
	return predicate.Change(sql.FieldIn(FieldTitle, vs...))
}

// TitleNotIn applies the NotIn predicate on the "Title" field.
func TitleNotIn(vs ...string) predicate.Change {
	return predicate.Change(sql.FieldNotIn(FieldTitle, vs...))
}

// TitleGT applies the GT predicate on the "Title" field.
func TitleGT(v string) predicate.Change {
	return predicate.Change(sql.FieldGT(FieldTitle, v))
}

// TitleGTE applies the GTE predicate on the "Title" field.
func TitleGTE(v string) predicate.Change {
	return predicate.Change(sql.FieldGTE(FieldTitle, v))
}

// TitleLT applies the LT predicate on the "Title" field.
func TitleLT(v string) predicate.Change {
	return predicate.Change(sql.FieldLT(FieldTitle, v))
}

// TitleLTE applies the LTE predicate on the "Title" field.
func TitleLTE(v string) predicate.Change {
	return predicate.Change(sql.FieldLTE(FieldTitle, v))
}

// TitleContains applies the Contains predicate on the "Title" field.
func TitleContains(v string) predicate.Change {
	return predicate.Change(sql.FieldContains(FieldTitle, v))
}

// TitleHasPrefix applies the HasPrefix predicate on the "Title" field.
func TitleHasPrefix(v string) predicate.Change {
	return predicate.Change(sql.FieldHasPrefix(FieldTitle, v))
}

// TitleHasSuffix applies the HasSuffix predicate on the "Title" field.
func TitleHasSuffix(v string) predicate.Change {
	return predicate.Change(sql.FieldHasSuffix(FieldTitle, v))
}

// TitleIsNil applies the IsNil predicate on the "Title" field.
func TitleIsNil() predicate.Change {
	return predicate.Change(sql.FieldIsNull(FieldTitle))
}

// TitleNotNil applies the NotNil predicate on the "Title" field.
func TitleNotNil() predicate.Change {
	return predicate.Change(sql.FieldNotNull(FieldTitle))
}

// TitleEqualFold applies the EqualFold predicate on the "Title" field.
func TitleEqualFold(v string) predicate.Change {
	return predicate.Change(sql.FieldEqualFold(FieldTitle, v))
}

// TitleContainsFold applies the ContainsFold predicate on the "Title" field.
func TitleContainsFold(v string) predicate.Change {
	return predicate.Change(sql.FieldContainsFold(FieldTitle, v))
}

// SummaryEQ applies the EQ predicate on the "Summary" field.
func SummaryEQ(v string) predicate.Change {
	return predicate.Change(sql.FieldEQ(FieldSummary, v))
}

// SummaryNEQ applies the NEQ predicate on the "Summary" field.
func SummaryNEQ(v string) predicate.Change {
	return predicate.Change(sql.FieldNEQ(FieldSummary, v))
}

// SummaryIn applies the In predicate on the "Summary" field.
func SummaryIn(vs ...string) predicate.Change {
	return predicate.Change(sql.FieldIn(FieldSummary, vs...))
}

// SummaryNotIn applies the NotIn predicate on the "Summary" field.
func SummaryNotIn(vs ...string) predicate.Change {
	return predicate.Change(sql.FieldNotIn(FieldSummary, vs...))
}

// SummaryGT applies the GT predicate on the "Summary" field.
func SummaryGT(v string) predicate.Change {
	return predicate.Change(sql.FieldGT(FieldSummary, v))
}

// SummaryGTE applies the GTE predicate on the "Summary" field.
func SummaryGTE(v string) predicate.Change {
	return predicate.Change(sql.FieldGTE(FieldSummary, v))
}

// SummaryLT applies the LT predicate on the "Summary" field.
func SummaryLT(v string) predicate.Change {
	return predicate.Change(sql.FieldLT(FieldSummary, v))
}

// SummaryLTE applies the LTE predicate on the "Summary" field.
func SummaryLTE(v string) predicate.Change {
	return predicate.Change(sql.FieldLTE(FieldSummary, v))
}

// SummaryContains applies the Contains predicate on the "Summary" field.
func SummaryContains(v string) predicate.Change {
	return predicate.Change(sql.FieldContains(FieldSummary, v))
}

// SummaryHasPrefix applies the HasPrefix predicate on the "Summary" field.
func SummaryHasPrefix(v string) predicate.Change {
	return predicate.Change(sql.FieldHasPrefix(FieldSummary, v))
}

// SummaryHasSuffix applies the HasSuffix predicate on the "Summary" field.
func SummaryHasSuffix(v string) predicate.Change {
	return predicate.Change(sql.FieldHasSuffix(FieldSummary, v))
}

// SummaryIsNil applies the IsNil predicate on the "Summary" field.
func SummaryIsNil() predicate.Change {
	return predicate.Change(sql.FieldIsNull(FieldSummary))
}

// SummaryNotNil applies the NotNil predicate on the "Summary" field.
func SummaryNotNil() predicate.Change {
	return predicate.Change(sql.FieldNotNull(FieldSummary))
}

// SummaryEqualFold applies the EqualFold predicate on the "Summary" field.
func SummaryEqualFold(v string) predicate.Change {
	return predicate.Change(sql.FieldEqualFold(FieldSummary, v))
}

// SummaryContainsFold applies the ContainsFold predicate on the "Summary" field.
func SummaryContainsFold(v string) predicate.Change {
	return predicate.Change(sql.FieldContainsFold(FieldSummary, v))
}

// CreatedAtEQ applies the EQ predicate on the "createdAt" field.
func CreatedAtEQ(v time.Time) predicate.Change {
	return predicate.Change(sql.FieldEQ(FieldCreatedAt, v))
}

// CreatedAtNEQ applies the NEQ predicate on the "createdAt" field.
func CreatedAtNEQ(v time.Time) predicate.Change {
	return predicate.Change(sql.FieldNEQ(FieldCreatedAt, v))
}

// CreatedAtIn applies the In predicate on the "createdAt" field.
func CreatedAtIn(vs ...time.Time) predicate.Change {
	return predicate.Change(sql.FieldIn(FieldCreatedAt, vs...))
}

// CreatedAtNotIn applies the NotIn predicate on the "createdAt" field.
func CreatedAtNotIn(vs ...time.Time) predicate.Change {
	return predicate.Change(sql.FieldNotIn(FieldCreatedAt, vs...))
}

// CreatedAtGT applies the GT predicate on the "createdAt" field.
func CreatedAtGT(v time.Time) predicate.Change {
	return predicate.Change(sql.FieldGT(FieldCreatedAt, v))
}

// CreatedAtGTE applies the GTE predicate on the "createdAt" field.
func CreatedAtGTE(v time.Time) predicate.Change {
	return predicate.Change(sql.FieldGTE(FieldCreatedAt, v))
}

// CreatedAtLT applies the LT predicate on the "createdAt" field.
func CreatedAtLT(v time.Time) predicate.Change {
	return predicate.Change(sql.FieldLT(FieldCreatedAt, v))
}

// CreatedAtLTE applies the LTE predicate on the "createdAt" field.
func CreatedAtLTE(v time.Time) predicate.Change {
	return predicate.Change(sql.FieldLTE(FieldCreatedAt, v))
}

// WarningEQ applies the EQ predicate on the "warning" field.
func WarningEQ(v bool) predicate.Change {
	return predicate.Change(sql.FieldEQ(FieldWarning, v))
}

// WarningNEQ applies the NEQ predicate on the "warning" field.
func WarningNEQ(v bool) predicate.Change {
	return predicate.Change(sql.FieldNEQ(FieldWarning, v))
}

// HasService applies the HasEdge predicate on the "service" edge.
func HasService() predicate.Change {
	return predicate.Change(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.Edge(sqlgraph.M2O, true, ServiceTable, ServiceColumn),
		)
		sqlgraph.HasNeighbors(s, step)
	})
}

// HasServiceWith applies the HasEdge predicate on the "service" edge with a given conditions (other predicates).
func HasServiceWith(preds ...predicate.Service) predicate.Change {
	return predicate.Change(func(s *sql.Selector) {
		step := newServiceStep()
		sqlgraph.HasNeighborsWith(s, step, func(s *sql.Selector) {
			for _, p := range preds {
				p(s)
			}
		})
	})
}

// And groups predicates with the AND operator between them.
func And(predicates ...predicate.Change) predicate.Change {
	return predicate.Change(sql.AndPredicates(predicates...))
}

// Or groups predicates with the OR operator between them.
func Or(predicates ...predicate.Change) predicate.Change {
	return predicate.Change(sql.OrPredicates(predicates...))
}

// Not applies the not operator on the given predicate.
func Not(p predicate.Change) predicate.Change {
	return predicate.Change(sql.NotPredicates(p))
}
