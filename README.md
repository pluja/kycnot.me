# kycnot.me source code

Source code of the [kycnot.me](https://kycnot.me) website (v2025).

![banner](/src/frontend/static/assets/banner.webp)

## Project structure

- `src`: Project code.
  - `frontend`: Generates the website HTML, CSS and JS.
    - `static`: Static files.
    - `templates`: [Templ](https://templ.guide/) templates.
  - `internal`: Internal backend packages:
    - `cache`: Cache implementation.
    - `database`: Database client.
    - `ent`: Database ent-go models.
    - `models`: Models.
    - `server`: Server/API implementation.
  - `utils`: Utilities.

## Tech Stack

- [Go](https://go.dev/)
  - [Templ](https://templ.guide/)
  - [ent-go](https://entgo.io/)
- [Tailwind CSS](https://tailwindcss.com/)
- [MariaDB](https://mariadb.org/)
- [Docker](https://www.docker.com/)
- [just](https://just.systems/)
